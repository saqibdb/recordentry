//
//  Test.swift
//  RecordEntry
//
//  Created by iBuildx-Macbook on 09/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

import UIKit
import SimpleImageViewer
import MessageUI

class Test: NSObject , MFMailComposeViewControllerDelegate {
    

    public func testFunc() {
        print("test for swift is done")
    }
    public func showTheFullImage2(customer : Customer , viewController : UIViewController){
        createEmailWithCustomer(customer: customer, viewController: viewController)
    }
    public func showTheFullImage(imageView : UIImageView , viewController : UIViewController){
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        viewController.present(imageViewerController, animated: true)
    }
    
    
    public func createEmailWithCustomer(customer : Customer , viewController : UIViewController){
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        else{
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([""])
            composeVC.setSubject("Customer Information")
            
            let emailBody = "<strong>First Name :</strong> \(customer.firstName!) <br><strong>Last Name :</strong> \(customer.lastName!) <br><strong>Address :</strong> \(customer.address!) <br><strong>Phone :</strong> \(customer.cellContact!) <br><strong>Profile Picture :</strong> <a>\(customer.profilePicture!) </a><br><strong>ID Picture :</strong><a> \(customer.idPicture!) </a><br>"
            
            composeVC.setMessageBody(emailBody, isHTML: true)
            
            // Present the view controller modally.
            viewController.present(composeVC, animated: true, completion: nil)

        }
    }
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        
        print("HERE")
        
        controller.dismiss(animated: true, completion: nil)
    }


}
