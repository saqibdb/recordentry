//
//  ILSCheck.m
//  RecordEntry
//
//  Created by iBuildX on 14/03/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "ILSCheck.h"
#import <CloudKit/CloudKit.h>
#import <CommonCrypto/CommonDigest.h>
#import "Utility.h"


const struct ILSCheckFields ILSCheckFields = {

    .bank = @"bank",
    .checkAmount = @"checkAmount",
    .checkNumber = @"checkNumber",
    .checkPhoto = @"checkPhoto",
    .checkType = @"checkType",
    .company = @"company",
    .dueAmount = @"dueAmount",
    .flatCharges = @"flatCharges",
    .isSync = @"isSync",
    .lastUpdate = @"lastUpdate",
    .netCharges = @"netCharges",
    .netDueAmount = @"netDueAmount",
    .ratedChargesType = @"ratedChargesType",
    .remarks = @"remarks",
    
    .customerId = @"customerId",
    
    
};
@implementation ILSCheck




#pragma mark - Lifecycle

- (instancetype _Nullable )initWithInputData:(id _Nullable )inputData{
    self = [super init];
    if (self) {
        [self mapObject:inputData];
    }
    
    return self;
}

#pragma mark - Private

-(void)mapObject:(CKRecord *)object {
    
    _bank = [object valueForKeyPath:ILSCheckFields.bank];
    _checkAmount = [object valueForKeyPath:ILSCheckFields.checkAmount];
    _checkNumber = [object valueForKeyPath:ILSCheckFields.checkNumber];
    _checkPhoto = [object valueForKeyPath:ILSCheckFields.checkPhoto];
    _checkType = [object valueForKeyPath:ILSCheckFields.checkType];
    _company = [object valueForKeyPath:ILSCheckFields.company];
    _dueAmount = [object valueForKeyPath:ILSCheckFields.dueAmount];
    _flatCharges = [object valueForKeyPath:ILSCheckFields.flatCharges];
    _isSync = [object valueForKeyPath:ILSCheckFields.isSync];
    _lastUpdate = [object valueForKeyPath:ILSCheckFields.lastUpdate];
    _netCharges = [object valueForKeyPath:ILSCheckFields.netCharges];
    _netDueAmount = [object valueForKeyPath:ILSCheckFields.netDueAmount];
    _ratedChargesType = [object valueForKeyPath:ILSCheckFields.ratedChargesType];
    _remarks = [object valueForKeyPath:ILSCheckFields.remarks];
    
    _customerId = [object valueForKeyPath:ILSCheckFields.customerId];

}
    
+ (NSMutableDictionary *_Nullable)createDictionaryFromCoreData :(ChecksCD *_Nullable)check andCustomerId :(NSString *)customerId{
    NSMutableDictionary *checkDict = [[NSMutableDictionary alloc] init];
    
    [checkDict setObject:@"Check" forKey:@"category"];
    
    
    [checkDict setObject:check.bank forKey:ILSCheckFields.bank];
    [checkDict setObject:check.checkAmount forKey:ILSCheckFields.checkAmount];
    [checkDict setObject:check.checkNumber forKey:ILSCheckFields.checkNumber];
    [checkDict setObject:check.checkPhoto forKey:ILSCheckFields.checkPhoto];
    [checkDict setObject:check.checkType forKey:ILSCheckFields.checkType];
    [checkDict setObject:check.company forKey:ILSCheckFields.company];
    [checkDict setObject:check.dueAmount forKey:ILSCheckFields.dueAmount];
    [checkDict setObject:check.flatCharges forKey:ILSCheckFields.flatCharges];

    [checkDict setObject:check.lastUpdate forKey:ILSCheckFields.lastUpdate];
    [checkDict setObject:check.netCharges forKey:ILSCheckFields.netCharges];
    [checkDict setObject:check.netDueAmount forKey:ILSCheckFields.netDueAmount];
    [checkDict setObject:check.ratedChargesType forKey:ILSCheckFields.ratedChargesType];
    [checkDict setObject:check.remarks forKey:ILSCheckFields.remarks];
    
    [checkDict setObject:customerId forKey:ILSCheckFields.customerId];

    return checkDict;
}

+(NSString *)MD5HexDigest:(NSData *)input {
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(input.bytes, (unsigned int)input.length, result);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for (int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+(ChecksCD *_Nullable)createCheckCDFromILS :(ILSCheck *_Nullable)checkILS {
    
    ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];

    newCheck.bank = checkILS.bank ;
    newCheck.checkAmount = checkILS.checkAmount ;
    newCheck.checkNumber = checkILS.checkNumber ;
    newCheck.checkPhoto = checkILS.checkPhoto ;
    newCheck.checkType = checkILS.checkType ;
    newCheck.company = checkILS.company ;
    newCheck.dueAmount = checkILS.dueAmount ;
    newCheck.flatCharges = checkILS.flatCharges ;
    newCheck.isSync = checkILS.isSync ;
    newCheck.lastUpdate = checkILS.lastUpdate ;
    newCheck.netCharges = checkILS.netCharges ;
    newCheck.netDueAmount = checkILS.netDueAmount ;
    newCheck.ratedChargesType = checkILS.ratedChargesType ;
    newCheck.remarks = checkILS.remarks ;
    
    
    return newCheck;
}


@end
