//
//  ILSCustomer.m
//  RecordEntry
//
//  Created by iBuildX on 14/03/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "ILSCustomer.h"
#import <CloudKit/CloudKit.h>
#import "Utility.h"

const struct ILSCustomerFields ILSCustomerFields = {

    .identificationNumber = @"identificationNumber",
    .socialSecurity = @"socialSecurity",
    .firstName = @"firstName",
    .lastName = @"lastName",
    .address = @"address",
    .city = @"city",
    .state = @"state",
    .zip = @"zip",
    
    .homeContact = @"homeContact",
    .officeContact = @"officeContact",
    .cellContact = @"cellContact",
    .remarks = @"remarks",
    .profilePicture = @"profilePicture",
    .idPicture = @"idPicture",
    .isBlackListed = @"isBlackListed",
    
    
    .objectId = @"objectId"
};

@implementation ILSCustomer


#pragma mark - Lifecycle

- (instancetype _Nullable )initWithInputData:(id _Nullable )inputData{
    self = [super init];
    if (self) {
        [self mapObject:inputData];
    }
    
    return self;
}

#pragma mark - Private

-(void)mapObject:(CKRecord *)object {
    

    
    
    
    _identificationNumber = [object valueForKeyPath:ILSCustomerFields.identificationNumber];
    _socialSecurity = [object valueForKeyPath:ILSCustomerFields.socialSecurity];
    _firstName = [object valueForKeyPath:ILSCustomerFields.firstName];
    _lastName = [object valueForKeyPath:ILSCustomerFields.lastName];
    _address = [object valueForKeyPath:ILSCustomerFields.address];
    _city = [object valueForKeyPath:ILSCustomerFields.city];
    _state = [object valueForKeyPath:ILSCustomerFields.state];
    _zip = [object valueForKeyPath:ILSCustomerFields.zip];
    
    _homeContact = [object valueForKeyPath:ILSCustomerFields.homeContact];
    _officeContact = [object valueForKeyPath:ILSCustomerFields.officeContact];
    _cellContact = [object valueForKeyPath:ILSCustomerFields.cellContact];
    _remarks = [object valueForKeyPath:ILSCustomerFields.remarks];
    _profilePicture = [object valueForKeyPath:ILSCustomerFields.profilePicture];
    _idPicture = [object valueForKeyPath:ILSCustomerFields.idPicture];
    _isBlackListed = [[object valueForKeyPath:ILSCustomerFields.isBlackListed] boolValue];
    
    
    _objectId = [object valueForKeyPath:ILSCustomerFields.objectId];

}


+(NSMutableDictionary *_Nullable)createDictionaryFromCoreData :(CustomerCD *_Nullable)customer{
    NSMutableDictionary *customerDict = [[NSMutableDictionary alloc] init];
    
    [customerDict setObject:@"Customer" forKey:@"category"];

    
    [customerDict setObject:customer.identificationNumber forKey:ILSCustomerFields.identificationNumber];
    [customerDict setObject:customer.socialSecurity forKey:ILSCustomerFields.socialSecurity];
    [customerDict setObject:customer.firstName forKey:ILSCustomerFields.firstName];
    [customerDict setObject:customer.lastName forKey:ILSCustomerFields.lastName];
    [customerDict setObject:customer.address forKey:ILSCustomerFields.address];
    [customerDict setObject:customer.city forKey:ILSCustomerFields.city];
    [customerDict setObject:customer.city forKey:ILSCustomerFields.state];
    [customerDict setObject:customer.zip forKey:ILSCustomerFields.zip];
    
    [customerDict setObject:customer.homeContact forKey:ILSCustomerFields.homeContact];
    [customerDict setObject:customer.officeContact forKey:ILSCustomerFields.officeContact];
    [customerDict setObject:customer.cellContact forKey:ILSCustomerFields.cellContact];
    [customerDict setObject:customer.remarks forKey:ILSCustomerFields.remarks];
    [customerDict setObject:customer.profilePicture forKey:ILSCustomerFields.profilePicture];
    [customerDict setObject:customer.idPicture forKey:ILSCustomerFields.idPicture];
    [customerDict setObject:[NSNumber numberWithBool:customer.isBlackListed] forKey:ILSCustomerFields.isBlackListed];
    
    
    //[customerDict setObject:customer.objectId forKey:ILSCustomerFields.objectId];
    
    
    
    return customerDict;
}
+(void)createCustomerCDFromILS :(ILSCustomer *_Nullable)customerILS withNewCreatedCustomerCD :(CustomerCD *)newCustomer{

    newCustomer.identificationNumber = customerILS.identificationNumber ;
    newCustomer.socialSecurity = customerILS.socialSecurity ;
    newCustomer.firstName = customerILS.firstName ;
    newCustomer.lastName = customerILS.lastName ;
    newCustomer.address = customerILS.address ;
    newCustomer.city = customerILS.city ;
    newCustomer.state = customerILS.state ;
    newCustomer.zip = customerILS.zip ;
    
    newCustomer.homeContact = customerILS.homeContact ;
    newCustomer.officeContact = customerILS.officeContact ;
    newCustomer.cellContact = customerILS.cellContact ;
    newCustomer.remarks = customerILS.remarks ;
    newCustomer.profilePicture = customerILS.profilePicture ;
    newCustomer.idPicture = customerILS.idPicture ;
    newCustomer.isBlackListed = customerILS.isBlackListed ;
    
    
    //newCustomer.objectId = customerILS.objectId ;
    
    
    
    
}
@end

