//
//  ILSCheck.h
//  RecordEntry
//
//  Created by iBuildX on 14/03/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>


extern const struct ILSCheckFields {
    
    
    __unsafe_unretained NSString * _Nullable bank;
    __unsafe_unretained NSString * _Nullable checkAmount;
    __unsafe_unretained NSString * _Nullable checkNumber;
    __unsafe_unretained NSString * _Nullable checkPhoto;
    __unsafe_unretained NSString * _Nullable checkType;
    __unsafe_unretained NSString * _Nullable company;
    __unsafe_unretained NSString * _Nullable dueAmount;
    __unsafe_unretained NSString * _Nullable flatCharges;
    __unsafe_unretained NSString * _Nullable isSync;
    __unsafe_unretained NSString * _Nullable lastUpdate;
    __unsafe_unretained NSString * _Nullable netCharges;
    __unsafe_unretained NSString * _Nullable netDueAmount;
    __unsafe_unretained NSString * _Nullable ratedChargesType;
    __unsafe_unretained NSString * _Nullable remarks;
    
    __unsafe_unretained NSString * _Nullable customerId;
    
    
} ILSCheckFields;




@interface ILSCheck : NSObject

@property (nullable, nonatomic, copy) NSString *bank;
@property (nullable, nonatomic, copy) NSString *checkAmount;
@property (nullable, nonatomic, copy) NSString *checkNumber;
@property (nullable, nonatomic, retain) NSData *checkPhoto;
@property (nullable, nonatomic, copy) NSString *checkType;
@property (nullable, nonatomic, copy) NSString *company;
@property (nullable, nonatomic, copy) NSString *dueAmount;
@property (nullable, nonatomic, copy) NSString *flatCharges;
@property (nonatomic) BOOL isSync;
@property (nullable, nonatomic, copy) NSDate *lastUpdate;
@property (nullable, nonatomic, copy) NSString *netCharges;
@property (nullable, nonatomic, copy) NSString *netDueAmount;
@property (nullable, nonatomic, copy) NSString *ratedChargesType;
@property (nullable, nonatomic, copy) NSString *remarks;
@property (nullable, nonatomic, copy) NSString *customerId;



- (instancetype _Nullable )initWithInputData:(id _Nullable )inputData;
+ (NSMutableDictionary *_Nullable)createDictionaryFromCoreData :(ChecksCD *_Nullable)check andCustomerId :(NSString *_Nullable)customerId;
+(NSString *_Nonnull)MD5HexDigest:(NSData *)input;
+(ChecksCD *_Nullable)createCheckCDFromILS :(ILSCheck *_Nullable)checkILS ;

@end
