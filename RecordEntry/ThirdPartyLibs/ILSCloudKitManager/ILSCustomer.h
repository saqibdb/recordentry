

//
//  ILSCustomer.h
//  RecordEntry
//
//  Created by iBuildX on 14/03/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>



extern const struct ILSCustomerFields {

    __unsafe_unretained NSString * _Nullable identificationNumber;
    __unsafe_unretained NSString * _Nullable socialSecurity;
    __unsafe_unretained NSString * _Nullable firstName;
    __unsafe_unretained NSString * _Nullable lastName;
    __unsafe_unretained NSString * _Nullable address;
    __unsafe_unretained NSString * _Nullable city;
    __unsafe_unretained NSString * _Nullable state;
    __unsafe_unretained NSString * _Nullable zip;
    
    __unsafe_unretained NSString * _Nullable homeContact;
    __unsafe_unretained NSString * _Nullable officeContact;
    __unsafe_unretained NSString * _Nullable cellContact;
    __unsafe_unretained NSString * _Nullable remarks;
    __unsafe_unretained NSString * _Nullable profilePicture;
    __unsafe_unretained NSString * _Nullable idPicture;
    __unsafe_unretained NSString * _Nullable isBlackListed;

    
    __unsafe_unretained NSString * _Nullable objectId;
    
} ILSCustomerFields;

@interface ILSCustomer : NSObject


@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *cellContact;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *homeContact;
@property (nullable, nonatomic, copy) NSString *identificationNumber;
@property (nullable, nonatomic, retain) NSData *idPicture;
@property (nonatomic) BOOL isBlackListed;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSDate *lastUpdate;
@property (nullable, nonatomic, copy) NSString *officeContact;
@property (nullable, nonatomic, retain) NSData *profilePicture;
@property (nullable, nonatomic, copy) NSString *remarks;
@property (nullable, nonatomic, copy) NSString *socialSecurity;
@property (nullable, nonatomic, copy) NSString *state;
@property (nullable, nonatomic, copy) NSString *zip;

@property (nullable, nonatomic, copy) NSString *objectId;


- (instancetype _Nullable )initWithInputData:(id _Nullable )inputData;


+(NSMutableDictionary *_Nullable)createDictionaryFromCoreData :(CustomerCD *_Nullable)customer;
+(void)createCustomerCDFromILS :(ILSCustomer *_Nullable)customerILS withNewCreatedCustomerCD :(CustomerCD *)newCustomer ;


@end

