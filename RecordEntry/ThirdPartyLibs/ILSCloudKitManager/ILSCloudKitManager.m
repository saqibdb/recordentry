//
//  ILSCloudKitManager.m
//  ILSCloudKitExample
//
//  Created by Hiran on 2/7/18.
//  Copyright © 2018 iLeaf Solutions pvt ltd. All rights reserved.
//

#import "ILSCloudKitManager.h"
#import <CloudKit/CloudKit.h>


@implementation ILSCloudKitManager


+ (CKDatabase *)publicCloudDatabase {
    return [[CKContainer defaultContainer] publicCloudDatabase];
}

// Retrieve existing records
+ (void)fetchAllRecordsWithType:(NSString*)recordType
                  withPredicate:(NSPredicate*)predicate
              CompletionHandler:(CloudKitCompletionHandler)handler {
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:recordType predicate:predicate];
    
    [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error) {
        
        if (!handler) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            handler (results, error);
        });
        
    }];
    
}
+ (void)fetchRecordsWithType:(NSString *)recordType completionHandler:(void (^)(NSArray *records, NSError *error))completionHandler {
    
    NSPredicate *truePredicate = [NSPredicate predicateWithValue:YES];
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:recordType
                                               predicate:truePredicate];
    
    CKQueryOperation *queryOperation = [[CKQueryOperation alloc] initWithQuery:query];
    
    NSMutableArray *results = [NSMutableArray new];
    
    queryOperation.recordFetchedBlock = ^(CKRecord *record) {
        
        
        [results addObject:record]; };
    
    queryOperation.queryCompletionBlock = ^(CKQueryCursor *cursor, NSError *error) {
        
        [self retrieveNextBatchOfQueryFromCursor:cursor
                                         results:results
                                           error:error
                               completionHandler:completionHandler]; };
    
    [[CKContainer defaultContainer].privateCloudDatabase addOperation:queryOperation];
    
}

+ (void)retrieveNextBatchOfQueryFromCursor:(CKQueryCursor *)cursor
                                   results:(NSMutableArray *)results
                                     error:(NSError *)error
                         completionHandler:(void (^)(NSArray *records, NSError *error))completionHandler {
    
    // CloudKit apparently has query limit
    
    if (cursor != nil
        && !error) {
        
        CKQueryOperation *nextOperation = [[CKQueryOperation alloc] initWithCursor:cursor];
        
        nextOperation.recordFetchedBlock = ^(CKRecord *record) {
            
            [results addObject:record]; };
        
        nextOperation.queryCompletionBlock = ^(CKQueryCursor *cursor, NSError *error) {
            
            [self retrieveNextBatchOfQueryFromCursor:cursor
                                             results:results
                                               error:error
                                   completionHandler:completionHandler]; };
        
        [[CKContainer defaultContainer].privateCloudDatabase addOperation:nextOperation];
        
        }
    else {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            completionHandler(results, error); }); }
    
}
/*

+(void)testCall {
    [self saveRecord:@[[self.ckManager createCKRecordForImage:self.imageDataAddedFromCamera]] withCompletionHandler:^(NSArray *records, NSError *error) {
        if (!error && records) {
            NSLog(@"INFO: Size of records array returned: %lu", (unsigned long)[records count]);
            CKRecord *record = [records lastObject];
            self.imageDataAddedFromCamera.recordID = record.recordID.recordName;
            NSLog(@"INFO: Record saved successfully for recordID: %@", self.imageDataAddedFromCamera.recordID);
            [self.hud dismiss:YES];
            [self.hud removeFromSuperview];
            [self.imageLoadManager addCIDForNewUserImage:self.imageDataAddedFromCamera]; // update the model with the new image
            // update number of items since array set has increased from new photo taken
            self.numberOfItemsInSection = [self.imageLoadManager.imageDataArray count];
            [self updateUI];
        } else {
            NSLog(@"Error trying to save the record!");
            NSLog(@"ERROR: Error saving record to cloud...%@", error.localizedDescription);
            [self.hud dismiss:YES];
            [self.hud removeFromSuperview];
            [self alertWithTitle:YIKES_TITLE andMessage:ERROR_SAVING_PHOTO_MSG];
        }
    }, recordProgressHandler:^(double progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Updating hud display...");
            [self.hud setProgress:progress animated:YES];
        });
    }];
}

*/

+ (void)saveRecords:(NSArray *)records withUpdate:(BOOL)isUpdate withCompletionHandler:(void (^)(NSArray *, NSError *))completionHandler recordProgressHandler:(void (^)(double))progressHandler {
    
    NSLog(@"INFO: Entered saveRecord...");
    
    CKModifyRecordsOperation *saveOperation = [[CKModifyRecordsOperation alloc] initWithRecordsToSave:records recordIDsToDelete:nil];
    
    if (isUpdate) {
        saveOperation.savePolicy=CKRecordSaveChangedKeys;
    }

    
    saveOperation.perRecordProgressBlock = ^(CKRecord *record, double progress) {
        if (progress <= 1) {
            NSLog(@"Save progress is: %f", progress);
            progressHandler(progress);
        }
    };
    
    saveOperation.perRecordCompletionBlock = ^(CKRecord *record, NSError *error) {
        NSLog(@"Save operation completed!");
        completionHandler(@[record], error);
    };
    
    [[CKContainer defaultContainer].privateCloudDatabase addOperation:saveOperation];
}



// add a new record
+ (void)createRecord:(NSDictionary *)fields
      WithRecordType:(NSString*)recordType
        WithRecordId:(NSString*)recordId
   completionHandler:(CloudKitCompletionHandler)handler {
    
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    CKRecord *record = [[CKRecord alloc] initWithRecordType:recordType recordID:recordID];
    
    [[fields allKeys] enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        record[key] = fields[key];
    }];
    
    CKDatabase *publicCloudDatabase = [[CKContainer defaultContainer] privateCloudDatabase];
    [publicCloudDatabase saveRecord:record completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!handler) return;
       
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler (nil, error);
            });
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"record saved successfully");
            handler (@[record], error);
        });
        
    }];
}


// updating the record by recordId
+ (void)updateRecord:(NSString *)recordId
          withRecord:(NSDictionary *)recordDic
   completionHandler:(CloudKitCompletionHandler)handler {
    
    // Fetch the record from the database
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    [publicDatabase fetchRecordWithID:recordID completionHandler:^(CKRecord *record, NSError *error) {
        if (!handler) return;
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler (nil, error);
            });
            return;
        }
        else {
            // Modify the record and save it to the database
            [[recordDic allKeys] enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
                
                record[key] = recordDic[key];
                
            }];
            
            [publicDatabase saveRecord:record completionHandler:^(CKRecord *savedRecord, NSError *saveError) {
                // Error handling for failed save to public database
                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        handler (nil, error);
                    });
                    return;
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"Saved successfully");
                        handler (@[record], error);
                    });
                }
            }];
        }
    }];
    
}



// remove the record
+ (void)removeRecordWithId:(NSString *)recordId
         completionHandler:(CloudKitCompletionHandler)handler {
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    [publicDatabase deleteRecordWithID:recordID completionHandler:^(CKRecordID *recordID, NSError *error) {
        if (!handler) return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler (nil, error);
        });
        
    }];
}


@end
