//
//  RMDemoStepController.m
//  RMStepsController-Demo
//
//  Created by Roland Moers on 14.11.13.
//  Copyright (c) 2013 Roland Moers
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "RMModalStepsController.h"
#import "EnrollNewTableViewController.h"
#import "AddNewCheckTableViewController.h"
#import "PhotocaptureViewController.h"
#import "CaptureChequePhotoView.h"

@interface RMModalStepsController ()

@end

@implementation RMModalStepsController

- (NSArray *)stepViewControllers {
    UIStoryboard *stepsStoryBoard = [UIStoryboard storyboardWithName:@"Steps" bundle:nil];

    UIViewController *firstStep = [stepsStoryBoard instantiateViewControllerWithIdentifier:@"IdentificationStep"];
    firstStep.step.title = @"Photo Identification";
    
    EnrollNewTableViewController *secondStep = [stepsStoryBoard instantiateViewControllerWithIdentifier:@"step1"];
    secondStep.step.title = @"Scan Customer ID";
    
    PhotocaptureViewController *thirdStep = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoStep"];
    thirdStep.step.title = @"Photo Customer";
    
    CaptureChequePhotoView *fourthStep = [self.storyboard instantiateViewControllerWithIdentifier:@"ChequePicStep"];
    fourthStep.step.title = @"Take Cheque Photo";
    
    AddNewCheckTableViewController *fifthStep = [self.storyboard instantiateViewControllerWithIdentifier:@"chequeInfoStep"];
    fifthStep.step.title = @"Cheque Information";
    
    
    //return @[firstStep, thirdStep, fourthStep , fifthStep];
    return @[firstStep, secondStep, thirdStep, fourthStep , fifthStep];
    //return @[secondStep];
}

- (void)finishedAllSteps {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)canceled {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
