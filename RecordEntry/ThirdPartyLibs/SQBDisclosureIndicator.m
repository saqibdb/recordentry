
//
//  SQBDisclosureIndicator.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 10/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "SQBDisclosureIndicator.h"

@interface SQBDisclosureIndicator ()

@property(nonatomic) CGFloat red;
@property(nonatomic) CGFloat green;
@property(nonatomic) CGFloat blue;
@property(nonatomic) CGFloat alpha;

@end

@implementation SQBDisclosureIndicator

//- (void)drawRect:(CGRect)rect {
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetRGBFillColor(context, self.red, self.green, self.blue, self.alpha);
//
//    CGContextMoveToPoint(context, 4, 0);
//    CGContextAddLineToPoint(context, 4, 0);
//    CGContextAddLineToPoint(context, 16, 12);
//    CGContextAddLineToPoint(context, 4, 24);
//    CGContextAddLineToPoint(context, 0, 24 - 2);
//    CGContextAddLineToPoint(context, 9, 12);
//    CGContextAddLineToPoint(context, 0, 4);
//    CGContextAddLineToPoint(context, 4, 0);
//    CGContextFillPath(context);
//}


- (void)drawRect:(CGRect)rect
{
    // (x,y) is the tip of the arrow
    CGFloat x = CGRectGetMaxX(self.bounds)-3.0;
    CGFloat y = CGRectGetMidY(self.bounds);
    const CGFloat R = 4.5;
    CGContextRef ctxt = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(ctxt, x-R, y-R);
    CGContextAddLineToPoint(ctxt, x, y);
    CGContextAddLineToPoint(ctxt, x-R, y+R);
    CGContextSetLineCap(ctxt, kCGLineCapSquare);
    CGContextSetLineJoin(ctxt, kCGLineJoinMiter);
    CGContextSetLineWidth(ctxt, 3);
    CGContextSetRGBFillColor(ctxt, self.red, self.green, self.blue, self.alpha);
    
    
    
    CGContextStrokePath(ctxt);
}

- (SQBDisclosureIndicator *)initWithColor:(UIColor *)color {
    self = [super initWithFrame:CGRectMake(0, 0, 16, 24)];
    self.backgroundColor = [UIColor clearColor];
    
    self.color = color;
    [self setNeedsDisplay];
    
    return self;
}

- (void)setColor:(UIColor *)color {
    _color = color;
    
    [self.color getRed:&_red green:&_green blue:&_blue alpha:&_alpha];
    
    [self setNeedsDisplay];
}

@end

