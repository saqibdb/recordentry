//
//  SQBDisclosureIndicator.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 10/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQBDisclosureIndicator : UIView

@property(nonatomic, strong) UIColor *color;

- (SQBDisclosureIndicator *)initWithColor:(UIColor *)color;

@end
