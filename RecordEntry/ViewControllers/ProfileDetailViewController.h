//
//  ProfileDetailViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKSHorizontalLineProgressView/MKSHorizontalLineProgressView.h>
#import "Customer.h"
#import <iCarousel/iCarousel.h>
#import "Utility.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>


@interface ProfileDetailViewController : UIViewController<iCarouselDelegate, iCarouselDataSource , UICollectionViewDelegate , UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UIImageView *idImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameTxt;
@property (weak, nonatomic) IBOutlet UILabel *addressTxt;
@property (weak, nonatomic) IBOutlet UILabel *stateTxt;
@property (weak, nonatomic) IBOutlet UILabel *cityTxt;
@property (weak, nonatomic) IBOutlet UILabel *zipTxt;
@property (weak, nonatomic) IBOutlet UILabel *dlIdTxt;
@property (weak, nonatomic) IBOutlet UILabel *ssnTxt;
@property (weak, nonatomic) IBOutlet UILabel *homeCnTxt;
@property (weak, nonatomic) IBOutlet UILabel *officeCnTxt;
@property (weak, nonatomic) IBOutlet UILabel *cellCnTxt;
@property (weak, nonatomic) IBOutlet UITextView *remarksTxt;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet MKSHorizontalLineProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIButton *customerHistorry;
@property (weak, nonatomic) IBOutlet UIButton *registerFingerPrint;


@property (weak, nonatomic) IBOutlet UILabel *firstTransactionText;
@property (weak, nonatomic) IBOutlet UILabel *lastTransactionText;
@property (weak, nonatomic) IBOutlet UILabel *totalTransactionsText;




@property (weak, nonatomic) IBOutlet iCarousel *checksCarousel;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *blackListBtn;

@property (weak, nonatomic) IBOutlet UILabel *firstNameText;
@property (weak, nonatomic) IBOutlet UILabel *lastNameText;
@property (weak, nonatomic) IBOutlet UILabel *addressText;

@property (weak, nonatomic) IBOutlet UILabel *officePhoneTxt;



@property (weak, nonatomic) IBOutlet UILabel *homePhoneTxt;

@property (weak, nonatomic) IBOutlet UILabel *cellPhoneTxt;




@property (weak, nonatomic) IBOutlet UICollectionView *checksCollectionView;


- (IBAction)cancelAction:(id)sender;


@property (strong, nonatomic) CustomerCD *customer;

@property BOOL isAddingCheck;

@property (weak, nonatomic) IBOutlet UIButton *addNewCheckSpecial;

- (IBAction)customerHistoryAction:(id)sender;
- (IBAction)registerFingerPrintAction:(id)sender;

- (IBAction)blackListAction:(id)sender;

- (IBAction)profilePictureAction:(UIButton *)sender;
- (IBAction)idCardPictureAction:(UIButton *)sender;
- (IBAction)addNewCheckSpecialAction:(UIButton *)sender;

- (IBAction)deleteCustomerAction:(UIButton *)sender;





@end
