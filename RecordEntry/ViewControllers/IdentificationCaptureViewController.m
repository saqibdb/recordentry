//
//  IdentificationCaptureViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "IdentificationCaptureViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <RMStepsController/RMStepsController.h>

@interface IdentificationCaptureViewController ()

@end

@implementation IdentificationCaptureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.cameraView initAgain];
    
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [self.cameraView stopCamera];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)captureAction:(id)sender {
    [self.cameraView takePhotoWithCompletion:^(UIImage *image) {
        
        if (image) {
            NSLog(@"Got the Image");

            //UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.cameraView.frame.size.width, self.cameraView.frame.size.height)];
            CGSize sz ;
            if (image.size.width < image.size.height) {
                sz = CGSizeMake(image.size.width, image.size.width);
            }
            else{
                sz = CGSizeMake(image.size.height, image.size.height);
            }
            sz = CGSizeMake(512.0, 384.0);
            //iv.image = [self squareImageWithImage:image scaledToSize:sz];
            self.identificationImage = [self squareImageWithImage:image scaledToSize:sz];
            [self.cameraView stopCamera];
            [self nextaction:nil];
            //[self.cameraView addSubview:iv];
        }
    }];
}

- (IBAction)nextaction:(id)sender {

    
    if (self.identificationImage) {
        [self.stepsController showNextStep];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Picture" andText:@"Please Take a Photo of Customer Identification to proceed." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
}





- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
