//
//  BlackListViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 12/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "BlackListViewController.h"
#import "Record.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "CustomerTableViewCell.h"
#import "Customer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

#import "ProfileDetailViewController.h"
#import "SQBDisclosureIndicator.h"


@interface BlackListViewController (){
    NSMutableArray *allCustomers;
    Customer *selectedCustomer;
}

@end

@implementation BlackListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.customerTableView.delegate = self;
    self.customerTableView.dataSource = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [self loadAllData];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


-(void)loadAllData
{
    if (!allCustomers.count) {
        [SVProgressHUD showWithStatus:@"Getting BlackListed Customers..."];
    }
    NSString *whereClause = [NSString stringWithFormat:@"isBlackListed = 'YES'"];
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setWhereClause:whereClause];
    
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    
    
    [dataStore find:queryBuilder
           response:^(NSArray *customers) {
               [SVProgressHUD dismiss];
        allCustomers = [[NSMutableArray alloc] initWithArray:customers];
        
        [self.customerTableView reloadData];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)filterDataWithSearchString:(NSString *)strSearch andColumn :(NSString *)column
{
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];
    NSString *whereClause = [NSString stringWithFormat:@"%@ LIKE '%%%@%%' AND isBlackListed = 'YES'",column, strSearch];
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setWhereClause:whereClause];
    
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore find:queryBuilder
           response:^(NSArray *customers) {
               NSLog(@"Result: %@", customers);
               
               [SVProgressHUD dismiss];
               allCustomers = [[NSMutableArray alloc] initWithArray:customers];
               
               [self.customerTableView reloadData];
           }
              error:^(Fault *error) {
                  NSLog(@"Server reported an error: %@", error);
                  [SVProgressHUD dismiss];
                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                  [alert show];
              }
     ];
}

#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allCustomers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CustomerTableViewCell";
    CustomerTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
    }
    
    
    Customer *customer = allCustomers[indexPath.row];
    cell.customerName.text = [NSString stringWithFormat:@"%@ %@",customer.firstName , customer.lastName];
    
    
    [cell.customerAvatar sd_setShowActivityIndicatorView:YES];
    [cell.customerAvatar sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.customerAvatar sd_setImageWithURL:[NSURL URLWithString:customer.profilePicture]
                           placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   cell.customerAvatar.image = image;
                               }
                               else{
                                   cell.customerAvatar.image = [UIImage imageNamed:@"avatarPH"];
                               }
                               [cell layoutIfNeeded];
                           }];
    cell.accessoryView = [[SQBDisclosureIndicator alloc] initWithColor:[UIColor whiteColor]];
    
    [cell layoutIfNeeded];
    return cell ;
}//ToProfileView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedCustomer = allCustomers[indexPath.row];
    [self performSegueWithIdentifier:@"ToProfileView" sender:self];
}


- (IBAction)searchButtonTextFieldClicked:(UITextField *)sender {
    [SVProgressHUD showWithStatus:@"Getting Customers..."];
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];
    
    if (sender.text.length > 0){
        NSArray *names = [sender.text componentsSeparatedByString:@" "];
        if(names.count){
            [self filterDataWithSearchString:names[0] andColumn:@"firstName"];
        }
        else{
            [self filterDataWithSearchString:sender.text andColumn:@"firstName"];
        }
        
    }
    else{
        [self loadAllData];
    }
    
    [sender resignFirstResponder];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToProfileView"]) {
        ProfileDetailViewController *vc = segue.destinationViewController;
        vc.customer = selectedCustomer;
    }
}

@end
