//
//  AboutUsViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 23/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACFloatingTextfield_Objc/ACFloatingTextField.h>


@interface AboutUsViewController : UIViewController


@property (weak, nonatomic) IBOutlet ACFloatingTextField *passwordTxt;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *confirmPasswordtxt;


- (IBAction)changePasswordAction:(UIButton *)sender;




@end
