//
//  HistoryViewController.m
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "HistoryViewController.h"
#import "RecordTableViewCell.h"
#import "Record.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "SQBDisclosureIndicator.h"

@interface HistoryViewController (){
    NSMutableArray *allFoundRecords;
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.recordTableView.delegate = self;
    self.recordTableView.dataSource = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}



#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [allFoundRecords count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"RecordTableViewCell";
    RecordTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[RecordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier];
    }
    
    Record *record = allFoundRecords[indexPath.row];
    

    
    cell.idText.text = [NSString stringWithFormat:@"Identification : %@" , record.identification];
    cell.phoneText.text = [NSString stringWithFormat:@"Phone Number : %@" , record.phoneNumber];
    cell.addressText.text = [NSString stringWithFormat:@"Address : %@" , record.address];
    cell.nameText.text = [NSString stringWithFormat:@"Full Name : %@" , record.fullName];
    cell.checqueText.text = [NSString stringWithFormat:@"Checque Number : %@" , record.checqueNumber];
    cell.accessoryView = [[SQBDisclosureIndicator alloc] initWithColor:[UIColor whiteColor]];

    
    [cell layoutIfNeeded];
    return cell ;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)searchAction:(UIButton *)sender {
    [[self view] endEditing:YES];


    [SVProgressHUD showWithStatus:@"Searching Records From Cloud..."];
    
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setPageSize:100];
    
    
    NSString *whereClause;
    
    if (self.searchSegment.selectedSegmentIndex == 0) {
        whereClause = [NSString stringWithFormat:@"identification LIKE '%%%@%%'" , self.searchTextField.text];
    }
    else if(self.searchSegment.selectedSegmentIndex == 1) {
        whereClause = [NSString stringWithFormat:@"phoneNumber LIKE '%%%@%%'" , self.searchTextField.text];
    }
    else if(self.searchSegment.selectedSegmentIndex == 2) {
        whereClause = [NSString stringWithFormat:@"address LIKE '%%%@%%'" , self.searchTextField.text];
    }
    else if(self.searchSegment.selectedSegmentIndex == 3) {
        whereClause = [NSString stringWithFormat:@"fullName LIKE '%%%@%%'" , self.searchTextField.text];
    }
    else if(self.searchSegment.selectedSegmentIndex == 4) {
        whereClause = [NSString stringWithFormat:@"checqueNumber LIKE '%%%@%%'" , self.searchTextField.text];
    }
    
    [queryBuilder setWhereClause:whereClause];
    
    
    
    [queryBuilder setSortBy:@[@"created DESC"]];
    id<IDataStore> dataStore = [backendless.data of:[Record class]];
    [dataStore find:queryBuilder response:^(NSArray *allSingleOils) {
        [SVProgressHUD dismiss];
        allFoundRecords = [[NSMutableArray alloc] initWithArray:allSingleOils];

        [self.recordTableView reloadData];

    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }] ;
    
    
}
@end
