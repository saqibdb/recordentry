//
//  HomeViewController.m
//  RecordEntry
//
//  Created by Maulik Vekariya on 12/26/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "HomeViewController.h"
#import <LLSlideMenu/LLSlideMenu.h>
#import "RecordEntry-Swift.h"
#import "ContactViewController.h"



#import "Customer.h"

#import "Checks.h"

#import "Utility.h"

#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>
#import "ILSCheck.h"
#import "ILSCustomer.h"
#import "ILSCloudKitManager.h"

#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <CloudKit/CloudKit.h>
#import <Backendless.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "RecordEntry-Swift.h"



@interface HomeViewController (){
    NSMutableArray *allCustomersInCoreData;
    
    NSMutableArray *allBackendlessCustomers;
    NSMutableArray *allBackendlessChecks;
    
    NSMutableArray *allCustomersChecksMapped;
    
}

@property (nonatomic, strong) LLSlideMenu *slideMenu;

@property (nonatomic, strong) UIPanGestureRecognizer *leftSwipe;
@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *percent;



@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentLevelKeyOldDataSyncCheck= @"RecordOldDataSync";
    
    
    if ([preferences objectForKey:currentLevelKeyOldDataSyncCheck] == nil){
        self.syncView.hidden = NO;
    }
    else if ([[preferences objectForKey:currentLevelKeyOldDataSyncCheck] isEqualToString:@"NO"]){
        self.syncView.hidden = NO;
    }
    else{
        NSLog(@"All Previous Data was already Synced with your iCloud.");
        
        self.syncView.hidden = YES;
    }
    
    
    
    self.title = @"Home";
    // Do any additional setup after loading the view.
    

    [self.view layoutIfNeeded];
    
    [self enableShadowforView:self.menuView1];
    
    _slideMenu = [[LLSlideMenu alloc] init];
    
    

    [self.view addSubview:_slideMenu];

    _slideMenu.ll_menuWidth = 300.f;

    _slideMenu.ll_menuBackgroundColor = self.navigationController.navigationBar.barTintColor;

    _slideMenu.ll_springDamping = 20;
    _slideMenu.ll_springVelocity = 15;
    _slideMenu.ll_springFramesNum = 60;
    
    

    /*
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(50, 40, 80, 80)];
    [img setImage:[UIImage imageNamed:@"launchPageImage"]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    // 添加触摸事件
    img.userInteractionEnabled = YES;
    [img addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTapAction)]];
    [_slideMenu addSubview:img];
    */
    
//    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 140, 150, 400)];
//    label.text = @"这是第一行菜单\n\n这是第二行菜单\n\n这是第三行菜单\n\n这是第四行菜单\n\n这是第五行菜单\n\n这是第六行菜单\n\n这是第七行菜单\n\n这是第八行菜单\n\n这是第九行菜单\n\n这是第十行菜单";
//    [label setTextColor:[UIColor whiteColor]];
//    [label setNumberOfLines:0];
//    [_slideMenu addSubview:label];
    
    
    
    UIView *menuView = [[[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil] objectAtIndex:0];

    [_slideMenu addSubview:menuView];

    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];

    
    
    
    
    
    
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];

    
    // Adding the swipe gesture on image view
    [menuView addGestureRecognizer:swipeLeft];
    menuView.userInteractionEnabled = YES;
    UIImageView *checkImageView = [menuView viewWithTag:11] ;
    checkImageView.userInteractionEnabled = YES;
    [checkImageView addGestureRecognizer:swipeLeft];

    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    singleTap.numberOfTapsRequired = 1;

    [checkImageView addGestureRecognizer:singleTap];
    
    //===================
    // 添加全屏侧滑手势
    //===================
    self.leftSwipe = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftHandle:)];
    self.leftSwipe.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:_leftSwipe];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [super viewDidAppear:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
- (IBAction)menu1:(DBTileButton *)sender {
    //[self performSegueWithIdentifier:@"customerPage" sender:self];
    
    
    [[Test new] testFunc];
}

- (IBAction)menu2:(DBTileButton *)sender {
    [self performSegueWithIdentifier:@"ToSteps" sender:self];
}

- (IBAction)menu3:(DBTileButton *)sender {
    [self performSegueWithIdentifier:@"customerPage" sender:self];
}

- (IBAction)menu4:(DBTileButton *)sender {
    [self performSegueWithIdentifier:@"ToPrivacy" sender:self];
}

- (IBAction)menu5:(id)sender {
    [self performSegueWithIdentifier:@"ToAbout" sender:self];

}
*/




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"customerPage"]) {
        ContactViewController *vc = segue.destinationViewController;
        vc.isAddingCheck = _isAddingCheck;
    }
    
}


- (void)swipeLeftHandle:(UIScreenEdgePanGestureRecognizer *)recognizer {
    // 如果菜单已打开则禁止滑动
    if (_slideMenu.ll_isOpen || _slideMenu.ll_isAnimating) {
        return;
    }
    // 计算手指滑的物理距离（滑了多远，与起始位置无关）
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    // 把这个百分比限制在 0~1 之间
    progress = MIN(1.0, MAX(0.0, progress));
    
    // 当手势刚刚开始，我们创建一个 UIPercentDrivenInteractiveTransition 对象
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.percent = [[UIPercentDrivenInteractiveTransition alloc] init];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        // 当手慢慢划入时，我们把总体手势划入的进度告诉 UIPercentDrivenInteractiveTransition 对象。
        [self.percent updateInteractiveTransition:progress];
        _slideMenu.ll_distance = [recognizer translationInView:self.view].x;
        
    } else if (recognizer.state == UIGestureRecognizerStateCancelled || recognizer.state == UIGestureRecognizerStateEnded) {
        // 当手势结束，我们根据用户的手势进度来判断过渡是应该完成还是取消并相应的调用 finishInteractiveTransition 或者 cancelInteractiveTransition 方法.
        if (progress > 0.4) {
            [self.percent finishInteractiveTransition];
            [_slideMenu ll_openSlideMenu];
        }else{
            [self.percent cancelInteractiveTransition];
            [_slideMenu ll_closeSlideMenu];
        }
        self.percent = nil;
    }
}



- (IBAction)menuAction:(id)sender {
    if (_slideMenu.ll_isOpen) {
        [_slideMenu ll_closeSlideMenu];
    } else {
        [_slideMenu ll_openSlideMenu];
    }
}

- (IBAction)menu1Action:(UIButton *)sender {
    self.isAddingCheck = NO;
    [self performSegueWithIdentifier:@"customerPage" sender:self];
}

- (IBAction)menu2Action:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToSteps" sender:self];
}

- (IBAction)menu3Action:(UIButton *)sender {
    self.isAddingCheck = YES;
    [self performSegueWithIdentifier:@"customerPage" sender:self];
}

- (IBAction)menu4Action:(UIButton *)sender {
    [self performSegueWithIdentifier:@"blacklistPage" sender:self];
}

- (IBAction)menu5Action:(UIButton *)sender {
}

- (IBAction)menu6Action:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToAbout" sender:self];
}

- (IBAction)menu7Action:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToAbout" sender:self];
}

- (IBAction)syncIcloudAction:(UIButton *)sender {
    
    
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Secret Password" message:@"Please input the Secret Password to Get the Data from Previous Cloud." preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *password = [[alertController textFields][0] text];
        NSLog(@"Current password %@", password);
        //compare the current password and do action here
        
        if ([password isEqualToString:@"test1234"]) {
            [self triggerSync];
            [SANotificationView showSAStatusBarBannerWithMessage:@"Sync Started. App will Notify once completed." backgroundColor:[UIColor greenColor] textColor:[UIColor blackColor] showTime:100];

        }
        else{
            [SANotificationView showSABannerWithTitle:@"Wrong Password" message:@"The Password You Entered is Not Correct." textColor:[UIColor whiteColor] image:[UIImage imageNamed:@"avatarPH"] backgroundColor:[UIColor redColor] showTime:5];
        }
    }];
    [alertController addAction:confirmAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Canelled");
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    return;
    
    NSLog(@"NEED DATA SYNCED HERE");
    
    
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == NO"];
    [fetchRequest setPredicate:predicate];
    
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    
    NSMutableArray *allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"firstName"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    allCustomers = [[allCustomers sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    
    
    //isSync
    
    
    NSLog(@"Total Customers Gotten = %lu" , (unsigned long)allCustomers.count);
    
    if (allCustomers.count) {
        [self saveCustomersInCloudWithCustomers:allCustomers withUpdate:NO];
        
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Data Is Already Synced" andText:@"The Data is Upto Date with your iCloud." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    
    
    
    
    
    
    //[self saveCustomerInCloudWithCustomer:allCustomers.firstObject andAnotherCustomer:allCustomers.lastObject];
    
    //[self fetchILSCustomers];
    
}

-(void)saveCustomersInCloudWithCustomers :(NSArray *)customers withUpdate:(BOOL)isUpdate{
    [SVProgressHUD showWithStatus:@"Saving..."];
    
    
    NSMutableArray *customerRecords = [[NSMutableArray alloc] init];
    
    
    for (CustomerCD *customer in customers) {
        NSLog(@"Customer Name = %@" , customer.firstName);
        NSDictionary *object = [ILSCustomer createDictionaryFromCoreData:customer];
        NSString *fullName = [ILSCheck MD5HexDigest:customer.idPicture];
        /*
        if (customer.backendlessId) {
            fullName = customer.backendlessId;
        }
        else{
            fullName = [fullName stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        }*/
        CKRecord *record1 = [[CKRecord alloc] initWithRecordType:@"Customer" recordID:[[CKRecordID alloc] initWithRecordName:fullName]];
        record1[@"identificationNumber"] = [object valueForKeyPath:ILSCustomerFields.identificationNumber];
        record1[@"socialSecurity"] = [object valueForKeyPath:ILSCustomerFields.socialSecurity];
        record1[@"firstName"] = [object valueForKeyPath:ILSCustomerFields.firstName];
        record1[@"lastName"] = [object valueForKeyPath:ILSCustomerFields.lastName];
        record1[@"address"] = [object valueForKeyPath:ILSCustomerFields.address];
        record1[@"city"] = [object valueForKeyPath:ILSCustomerFields.city];
        record1[@"state"] = [object valueForKeyPath:ILSCustomerFields.state];
        record1[@"zip"] = [object valueForKeyPath:ILSCustomerFields.zip];
        
        record1[@"homeContact"] = [object valueForKeyPath:ILSCustomerFields.homeContact];
        record1[@"officeContact"] = [object valueForKeyPath:ILSCustomerFields.officeContact];
        record1[@"cellContact"] = [object valueForKeyPath:ILSCustomerFields.cellContact];
        record1[@"remarks"]= [object valueForKeyPath:ILSCustomerFields.remarks];
        record1[@"profilePicture"] = [object valueForKeyPath:ILSCustomerFields.profilePicture];
        record1[@"idPicture"] = [object valueForKeyPath:ILSCustomerFields.idPicture];
        record1[@"isBlackListed"] = [object valueForKeyPath:ILSCustomerFields.isBlackListed];
        record1[@"objectId"] = [object valueForKeyPath:ILSCustomerFields.objectId];
        
        [customerRecords addObject:record1];
    }

    [ILSCloudKitManager saveRecords:customerRecords withUpdate:isUpdate withCompletionHandler:^(NSArray *savedRecords, NSError *error) {
        [SVProgressHUD dismiss];
        if(error) {
            NSLog(@"%@", error);
        } else {
            NSLog(@"Saved successfully");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                for (CustomerCD *customer in customers) {
                    customer.isSync = YES;
                    customer.uploadStatus = @"synced";
                }
                NSError *coreDataError;
                [[Utility managedObjectContext] save:&coreDataError];
                if(coreDataError){
                    NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE= %@" , coreDataError.description);
                }
                else{
                    NSLog(@"DATA SAVED AT AFTER CLOUD SAVE");
                }
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Saved" andText:@"Successfully Saved." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
            });
        }
    } recordProgressHandler:^(double progress) {
        NSLog(@"Progress = %f" , progress);
    }];
    
    
    /*
    
    [ILSCloudKitManager createRecord:object WithRecordType:@"Customer" WithRecordId:fullName completionHandler:^(NSArray *results, NSError *error) {
        [SVProgressHUD dismiss];
        if(error) {
            NSLog(@"%@", error);
        } else {
            NSLog(@"Saved successfully");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Saved" andText:@"Successfully Saved." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
            });
        }
        
    }];
     */
}



- (void)fetchILSCustomers {
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    NSString *kCKRecordName = @"Customer";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@", kCKRecordName];
    
    
    
    [ILSCloudKitManager fetchRecordsWithType:kCKRecordName completionHandler:^(NSArray *records, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            // Error handling for failed fetch from public database
            
            NSLog(@"ERROR AT ICLOUD = %@ %ld" , error.localizedDescription , (long)error.code);
        }
        else {
            // Display the fetched records
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //NSArray *students = [self mapStudents:results];
                NSArray *customers = [self mapCustomers:records];

                
                NSLog(@"Results Gotten From ILS = %lu" , (unsigned long)customers.count);
                
                
                for (ILSCustomer *customer in customers) {
                    NSLog(@"Name is %@ %@" , customer.firstName, customer.lastName);
                }

            });
        }
    }];
    
    /*
    [ILSCloudKitManager fetchAllRecordsWithType:kCKRecordName withPredicate:predicate CompletionHandler:^(NSArray *results, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (error) {
            // Error handling for failed fetch from public database
            
            NSLog(@"ERROR AT ICLOUD = %@ %ld" , error.localizedDescription , (long)error.code);
        }
        else {
            // Display the fetched records
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //NSArray *students = [self mapStudents:results];
                NSLog(@"Results Gotten From Ils = %lu" , (unsigned long)results.count);
                
            });
        }
    }];
    */
    
    
}

#pragma mark - Map fetch result to student model object

- (NSArray *)mapCustomers:(NSArray *)customers {
    
    if (customers.count == 0) return nil;
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    [customers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        ILSCustomer *customer = [[ILSCustomer alloc] initWithInputData:obj];
        [temp addObject:customer];
    }];
    
    return [NSArray arrayWithArray:temp];
}


- (void)imageViewTapAction {
    [_slideMenu ll_closeSlideMenu];
}
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    NSLog(@"Acted");
    [_slideMenu ll_closeSlideMenu];
}



-(void) enableShadowforView :(UIView *)view {
    //shadow part
    
    
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, 13);
    view.layer.shadowOpacity = 0.4;
    view.layer.shadowRadius = 3.0;
    
    
}


#pragma mark - Sync From Backendless Functions


-(void)triggerSync {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self syncFromBackendless];
}

-(void)getAllCustomers {
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setPageSize:100];
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore find:queryBuilder response:^(NSArray *customers) {
        NSLog(@"Customers Gotten = %lu" , (unsigned long)customers.count);
        //[self getAllChecksWithAllCustomers:customers];
        
        allBackendlessCustomers = [[NSMutableArray alloc] initWithArray:customers];
        [self getAllChecks];
        
    } error:^(Fault *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)getAllChecks {
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    DataQueryBuilder *query = [DataQueryBuilder new];
    [query setPageSize:100];
    [dataStore find:query response:^(NSArray *checks) {
        
        NSLog(@"Checks Gotten = %lu" , (unsigned long)checks.count);
        NSLog(@"allCustomersInCoreData Gotten = %lu" , (unsigned long)allCustomersInCoreData.count);
        
        allBackendlessChecks = [[NSMutableArray alloc] initWithArray:checks];
        
        [self mapCustomerWithCheck];
        
        
    } error:^(Fault *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}


-(void)mapCustomerWithCheck {
    
    allCustomersChecksMapped = [[NSMutableArray alloc] init];
    
    for (int i = 0 ; i < allBackendlessCustomers.count ; i++) {
        
        
        Customer *customer = allBackendlessCustomers[i];
        
        
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.customerId contains[cd] %@",customer.objectId];
        NSArray *filteredCkecks = [allBackendlessChecks filteredArrayUsingPredicate:bPredicate];
        
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.firstName contains[cd] %@" , customer.firstName];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.lastName contains[cd] %@", customer.lastName];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        
        NSArray *filteredCustomers = [allCustomersInCoreData filteredArrayUsingPredicate:predicate];
        
        
        if (filteredCustomers.count) {
            NSLog(@"Customer Already Exists = %@" , customer.firstName);
        }
        else{
            
            NSDictionary *mappedDict = @{ @"customer" : customer, @"checks" : filteredCkecks};
            
            [allCustomersChecksMapped addObject:mappedDict];
            
            //[self saveCustomerInCoreDataWithCustomer:customer andChecks:filteredCkecks];
        }
        
        if ((i+1) == allBackendlessCustomers.count) {
            [self saveInCoreDataWithCustomer:0];
            break;
        }
        else{
            NSLog(@"Total Count = %lu and index = %i" ,(unsigned long)allBackendlessCustomers.count , i );
        }
    }
}


-(void)saveInCoreDataWithCustomer :(int)index {
    
    if (index >= allCustomersChecksMapped.count) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"All Previous Data has been Synced with your iCloud." andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
        
        
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *currentLevelKeyOldDataSyncCheck= @"RecordOldDataSync";
        
        
        [preferences setObject:@"YES" forKey:currentLevelKeyOldDataSyncCheck];
        
        [preferences synchronize];
        
        
        
        return;
    }
    
    NSDictionary *customerDict = [allCustomersChecksMapped objectAtIndex:index];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    
    Customer *customer = [customerDict objectForKey:@"customer"];
    
    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];
    newCustomer.identificationNumber =  customer.identificationNumber;
    newCustomer.socialSecurity =  customer.socialSecurity;
    newCustomer.firstName =  customer.firstName;
    newCustomer.lastName =  customer.lastName;
    newCustomer.address =  customer.address;
    newCustomer.city =  customer.city;
    newCustomer.state =  customer.state;
    newCustomer.zip =  customer.zip;
    newCustomer.homeContact =  customer.homeContact;
    newCustomer.officeContact =  customer.officeContact;
    newCustomer.cellContact =  customer.cellContact;
    newCustomer.remarks =  customer.remarks;
    newCustomer.isBlackListed = [customer.isBlackListed containsString:@"YES"];
    newCustomer.isSync = NO;
    newCustomer.lastUpdate = [NSDate date];
    newCustomer.backendlessId = customer.objectId;
    newCustomer.uploadStatus = @"new";
    
    
    
    NSLog(@"Called Before ImageDownload");
    
    
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.idPicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image && finished) {
            // Cache image to disk or memory
            
            NSLog(@"Size of Image Downloaded (bytes):%lu",(unsigned long)[data length]);
            if (!error) {
                newCustomer.idPicture = UIImageJPEGRepresentation(image, 0.5);
                newCustomer.backendlessId = [ILSCheck MD5HexDigest:newCustomer.idPicture];

            }
            else{
                
            }
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.profilePicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if (image && finished) {
                    // Cache image to disk or memory
                    
                    NSLog(@"Size of Image Downloaded profilePicture(bytes):%lu",(unsigned long)[data length]);
                    if (!error) {
                        newCustomer.profilePicture = UIImageJPEGRepresentation(image, 0.5);
                    }
                    else{
                        NSLog(@"ERROR = %@" , error.description);
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    }
                    
                    NSError *coreDataError;
                    [[Utility managedObjectContext] save:&coreDataError]; //CUSTOMER SAVING
                    
                    
                    NSDictionary *customerObj = [ILSCustomer createDictionaryFromCoreData:newCustomer];
                    __block NSString *fullName = [ILSCheck MD5HexDigest:newCustomer.idPicture];
                    
                    
                    
                    
                    
                    [ILSCloudKitManager createRecord:customerObj WithRecordType:@"Customer" WithRecordId:fullName completionHandler:^(NSArray *results, NSError *error) {
                        if(error) {
                            NSLog(@"+++++++++++%@ %li", error.localizedDescription , (long)error.code);
                            if (error.code == 14) {
                                newCustomer.isSync = YES;
                                newCustomer.uploadStatus = @"synced";
                            }
                            else{
                                newCustomer.isSync = NO;
                            }
                        } else {
                            NSLog(@"Saved successfully");
                            newCustomer.isSync = YES;
                            newCustomer.uploadStatus = @"synced";
                        }
                        NSError *coreDataError;
                        [[Utility managedObjectContext] save:&coreDataError];
                        if(coreDataError){
                            NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE= %@" , coreDataError.description);
                        }
                        else{
                            NSLog(@"DATA SAVED AT AFTER CLOUD SAVE");
                        }
                    }];
                    
                    if(coreDataError){
                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                        
                    }
                    else{
                        NSLog(@"DATA SAVED");
                        
                        NSMutableArray *checks = [customerDict objectForKey:@"checks"];
                        __block int checker = 0;
                        
                        
                        for (int j = 0 ; j < checks.count ; j++) {
                            Checks *check = [checks objectAtIndex:j];
                            ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];
                            
                            newCheck.bank = check.bank;
                            newCheck.checkAmount = check.checkAmount;
                            newCheck.checkNumber = check.checkNumber;
                            newCheck.checkType = check.checkType;
                            newCheck.company = check.company;
                            
                            newCheck.dueAmount = check.dueAmount;
                            newCheck.flatCharges = check.flatCharges;
                            newCheck.netCharges = check.netCharges;
                            newCheck.netDueAmount = check.netDueAmount;
                            newCheck.ratedChargesType = check.ratedChargesType;
                            newCheck.isSync = NO;
                            newCheck.lastUpdate = check.created;
                            newCheck.remarks = check.remarks;
                            newCheck.backendlessId = check.objectId;
                            newCheck.uploadStatus = @"new";
                            checker = checker + 1;
                            
                            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:check.frontPhoto] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                
                                if (image && finished) {
                                    // Cache image to disk or memory
                                    
                                    NSLog(@"Size of Image Downloaded checPhoto(bytes):%lu",(unsigned long)[data length]);
                                    if (!error) {
                                        newCheck.checkPhoto = UIImageJPEGRepresentation(image, 0.8);
                                        newCheck.backendlessId = [ILSCheck MD5HexDigest:newCheck.checkPhoto];

                                    }
                                    else{
                                        NSLog(@"ERROR = %@" , error.description);
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                    }
                                    [newCustomer addChecksObject:newCheck];
                                    
                                    NSError *coreDataErrorChecks;
                                    [[Utility managedObjectContext] save:&coreDataErrorChecks];
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    NSDictionary *checkObject = [ILSCheck createDictionaryFromCoreData:newCheck andCustomerId:fullName];
                                    
                                    [ILSCloudKitManager createRecord:checkObject WithRecordType:@"Check" WithRecordId:[ILSCheck MD5HexDigest:newCheck.checkPhoto] completionHandler:^(NSArray *results, NSError *error) {
                                        if(error) {
                                            NSLog(@"%@", error);
                                        } else {
                                            NSLog(@"Saved successfully");
                                        }
                                        newCheck.isSync = YES;
                                        newCheck.uploadStatus = @"synced";
                                        NSError *coreDataError;
                                        [[Utility managedObjectContext] save:&coreDataError];
                                        if(coreDataError){
                                            NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE CHECK= %@" , coreDataError.description);
                                        }
                                        else{
                                            NSLog(@"DATA SAVED AT AFTER CLOUD SAVE CHECK");
                                        }
                                    }];
                                    
                                    
                                    
                                    
                                    
                                    if(coreDataErrorChecks){
                                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                        checker = checker - 1;
                                        
                                        
                                    }
                                    else{
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                        NSLog(@"DATA CEHCKS SAVED");
                                        [[NSNotificationCenter defaultCenter]postNotificationName:@"coreDataUpdatedRecordEntry" object:nil];
                                        checker = checker - 1;
                                    }
                                    
                                    if (checker == 0) {
                                        
                                        
                                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15.0 * NSEC_PER_SEC));
                                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            [self saveInCoreDataWithCustomer:(index + 1)];
                                        });
                                    }
                                    
                                }
                                else{
                                    NSLog(@"Error At Image Upload = %@",error.localizedDescription);
                                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                    
                                }
                            }];
                        }
                    }
                    
                }
                else{
                    NSLog(@"Error At Image Upload = %@",error.localizedDescription);
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    
                }
            }];
            
        }
        else{
            NSLog(@"Error At Image Upload = %@",error.localizedDescription);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
        }
    }];
}




-(void)syncFromBackendless {
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    allCustomersInCoreData = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self getAllCustomers];
}





@end
