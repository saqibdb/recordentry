//
//  PhotocaptureViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 14/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDPhotoCameraView.h"
#import "Customer.h"



@interface PhotocaptureViewController : UIViewController

@property (weak, nonatomic) IBOutlet BDPhotoCameraView *cameraView;

@property (weak, nonatomic) IBOutlet UIButton *captureBtn;
    @property (weak, nonatomic) IBOutlet UIButton *previousBTn;
    @property (weak, nonatomic) IBOutlet UIButton *nextbtn;
    
@property (strong, nonatomic) Customer *customer;

@property (strong, nonatomic) UIImage *customerPhoto;

    
    
    
- (IBAction)captureAction:(id)sender;
- (IBAction)previousAction:(id)sender;
- (IBAction)nextAction:(id)sender;
    
    
    
    
@end
