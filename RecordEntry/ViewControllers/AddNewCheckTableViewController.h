//
//  AddNewCheckTableViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DVSwitch/DVSwitch.h>
#import <ACFloatingTextfield_Objc/ACFloatingTextField.h>
#import "Utility.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>


@interface AddNewCheckTableViewController : UITableViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *switchView;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *customerNameText;

@property (weak, nonatomic) IBOutlet ACFloatingTextField *checkNumberText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *checkAmountText;


@property (weak, nonatomic) IBOutlet ACFloatingTextField *companyText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *bankText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *checkTypeText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *remarksText;


@property (weak, nonatomic) IBOutlet UIImageView *frontPhotoImageView;
@property (weak, nonatomic) IBOutlet UIButton *frontBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backPhotoImageView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet ACFloatingTextField *ratedChargeValueText;

@property (weak, nonatomic) IBOutlet UILabel *flatChargesText;

@property (weak, nonatomic) IBOutlet UILabel *checkAmountText2;
@property (weak, nonatomic) IBOutlet UILabel *netChargesText;
@property (weak, nonatomic) IBOutlet UILabel *dueAmountText;

@property (weak, nonatomic) IBOutlet UILabel *netDueAmount;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;



@property (strong, nonatomic) CustomerCD *customer;


- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)checkAmountChanged:(UITextField *)sender;
- (IBAction)addFrontPhotoAction:(UIButton *)sender;



@end
