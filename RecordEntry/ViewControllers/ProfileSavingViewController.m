//
//  ProfileSavingViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ProfileSavingViewController.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>





@interface ProfileSavingViewController ()

@end


@implementation ProfileSavingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Confirm";

    self.idImageView.image = self.identificationImage;
    self.profileImageView.image = self.profileImage;
    self.nameTxt.text = [NSString stringWithFormat:@"%@ %@",self.customer.firstName , self.customer.lastName];
    self.addressTxt.text = self.customer.address;
    self.stateTxt.text = self.customer.state;
    self.cityTxt.text = self.customer.city;
    self.zipTxt.text = self.customer.zip;
    self.dlIdTxt.text = self.customer.identificationNumber;
    self.ssnTxt.text = self.customer.socialSecurity;

    self.homeCnTxt.text = self.customer.homeContact;
    self.officeCnTxt.text = self.customer.officeContact;
    self.cellCnTxt.text = self.customer.cellContact;
    self.remarksTxt.text = self.customer.remarks;

    
    
    
    
    self.progressView.progressValue = 0.0f;
    self.progressView.trackColor = [UIColor colorWithRed:34.0f/255.0f green:34.0f/255.0f blue:34.0f/255.0f alpha:1.0f];
    self.progressView.barColor = [UIColor colorWithRed:58.0/255.0 green:170.0/255.0 blue:53.0/255.0 alpha:1];
    self.progressView.barThickness = self.progressView.frame.size.height;
    self.progressView.showPercentageText = YES;
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAction:(id)sender {
    if ((self.customer != nil) && (self.identificationImage != nil) && (self.profileImage != nil)) {
        [SVProgressHUD showWithStatus:@"Uploading Identification Image on Cloud..."];
        self.progressView.progressValue = 10.0f;

        NSData *imageData = UIImageJPEGRepresentation(self.identificationImage, 0.8);
        NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
        NSString *filePathToBeSaved = [NSString stringWithFormat:@"/IdentificationPictures/%fId.jpg",epoch];
        [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
            [SVProgressHUD dismiss];
            self.customer.idPicture = newFile.fileURL;
            [self uploadProfilePicture];
            
        } error:^(Fault *fileError) {
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
    }
}

-(void)uploadProfilePicture {
    [SVProgressHUD showWithStatus:@"Uploading Profile Image on Cloud..."];
    self.progressView.progressValue = 50.0f;

    NSData *imageData = UIImageJPEGRepresentation(self.profileImage, 0.8);
    NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
    NSString *filePathToBeSaved = [NSString stringWithFormat:@"/ProfilePictures/%fProfile.jpg",epoch];
    [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
        [SVProgressHUD dismiss];
        self.customer.profilePicture = newFile.fileURL;
        [self createCustomer];
        
    } error:^(Fault *fileError) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)createCustomer {
    [SVProgressHUD showWithStatus:@"Creating New Customer on Cloud..."];
    self.progressView.progressValue = 90.0f;
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore save:self.customer response:^(id newCreatedRecord) {
        [SVProgressHUD dismiss];
        self.progressView.progressValue = 100.0f;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"A new Customer Has been Created in the Cloud. Please check the Search tab to search for it" andCancelButton:NO forAlertType:AlertSuccess];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [alert show];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}




- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
