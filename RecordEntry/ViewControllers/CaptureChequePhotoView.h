//
//  CaptureChequePhotoView.h
//  RecordEntry
//
//  Created by Maulik Vekariya on 12/28/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDPhotoCameraView.h"
#import "Customer.h"

@interface CaptureChequePhotoView : UIViewController

@property (weak, nonatomic) IBOutlet BDPhotoCameraView *cameraView;

@property (weak, nonatomic) IBOutlet UIButton *captureBtn;
@property (weak, nonatomic) IBOutlet UIButton *previousBTn;
@property (weak, nonatomic) IBOutlet UIButton *nextbtn;

@property (strong, nonatomic) Customer *customer;


@property (strong, nonatomic) UIImage *checkPhoto;



- (IBAction)captureAction:(id)sender;
- (IBAction)previousAction:(id)sender;
- (IBAction)nextAction:(id)sender;

@end
