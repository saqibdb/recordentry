//
//  AboutUsViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 23/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "AboutUsViewController.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>



#import <Backendless.h>
#import "Customer.h"

#import "Checks.h"

#import "Utility.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>




@interface AboutUsViewController (){
    NSMutableArray *allCustomersInCoreData;
    
    NSMutableArray *allBackendlessCustomers;
    NSMutableArray *allBackendlessChecks;
    
    NSMutableArray *allCustomersChecksMapped;

}

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





-(void)getAllCustomers {
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setPageSize:100];
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore find:queryBuilder response:^(NSArray *customers) {
        NSLog(@"Customers Gotten = %lu" , (unsigned long)customers.count);
        //[self getAllChecksWithAllCustomers:customers];
        
        allBackendlessCustomers = [[NSMutableArray alloc] initWithArray:customers];
        [self getAllChecks];
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)getAllChecks {
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    DataQueryBuilder *query = [DataQueryBuilder new];
    [query setPageSize:100];
    [dataStore find:query response:^(NSArray *checks) {

        NSLog(@"Checks Gotten = %lu" , (unsigned long)checks.count);
        NSLog(@"allCustomersInCoreData Gotten = %lu" , (unsigned long)allCustomersInCoreData.count);
        
        allBackendlessChecks = [[NSMutableArray alloc] initWithArray:checks];

        [self mapCustomerWithCheck];
        
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}


-(void)mapCustomerWithCheck {
    
    allCustomersChecksMapped = [[NSMutableArray alloc] init];
    
    for (int i = 0 ; i < allBackendlessCustomers.count ; i++) {
        
        
        Customer *customer = allBackendlessCustomers[i];
        
        
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.customerId contains[cd] %@",customer.objectId];
        NSArray *filteredCkecks = [allBackendlessChecks filteredArrayUsingPredicate:bPredicate];
        
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.firstName contains[cd] %@" , customer.firstName];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.lastName contains[cd] %@", customer.lastName];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        
        NSArray *filteredCustomers = [allCustomersInCoreData filteredArrayUsingPredicate:predicate];
        
        
        if (filteredCustomers.count) {
            NSLog(@"Customer Already Exists = %@" , customer.firstName);
        }
        else{
            
            NSDictionary *mappedDict = @{ @"customer" : customer, @"checks" : filteredCkecks};
            
            [allCustomersChecksMapped addObject:mappedDict];
            
            //[self saveCustomerInCoreDataWithCustomer:customer andChecks:filteredCkecks];
        }
        
        if ((i+1) == allBackendlessCustomers.count) {
            [self saveInCoreDataWithCustomer:0];
            break;
        }
        else{
            NSLog(@"Total Count = %lu and index = %i" ,(unsigned long)allBackendlessCustomers.count , i );
        }
    }
}


-(void)saveInCoreDataWithCustomer :(int)index {
    
    if (index >= allCustomersChecksMapped.count) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"All Previous Data has been Synced with your iCloud." andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
        return;
    }
    
    NSDictionary *customerDict = [allCustomersChecksMapped objectAtIndex:index];
    
    [SVProgressHUD showWithStatus:@"Syncing Data..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    
    Customer *customer = [customerDict objectForKey:@"customer"];
    
    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];
    newCustomer.identificationNumber =  customer.identificationNumber;
    newCustomer.socialSecurity =  customer.socialSecurity;
    newCustomer.firstName =  customer.firstName;
    newCustomer.lastName =  customer.lastName;
    newCustomer.address =  customer.address;
    newCustomer.city =  customer.city;
    newCustomer.state =  customer.state;
    newCustomer.zip =  customer.zip;
    newCustomer.homeContact =  customer.homeContact;
    newCustomer.officeContact =  customer.officeContact;
    newCustomer.cellContact =  customer.cellContact;
    newCustomer.remarks =  customer.remarks;
    newCustomer.isBlackListed = [customer.isBlackListed containsString:@"YES"];
    newCustomer.isSync = NO;
    newCustomer.lastUpdate = [NSDate date];
    newCustomer.uploadStatus = @"new";

    
    
    NSLog(@"Called Before ImageDownload");

    
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.idPicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image && finished) {
            // Cache image to disk or memory
            
            NSLog(@"Size of Image Downloaded (bytes):%lu",(unsigned long)[data length]);
            if (!error) {
                newCustomer.idPicture = UIImageJPEGRepresentation(image, 0.5);
            }
            else{
                
            }
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.profilePicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if (image && finished) {
                    // Cache image to disk or memory
                    
                    NSLog(@"Size of Image Downloaded profilePicture(bytes):%lu",(unsigned long)[data length]);
                    if (!error) {
                        newCustomer.profilePicture = UIImageJPEGRepresentation(image, 0.5);
                    }
                    else{
                        NSLog(@"ERROR = %@" , error.description);
                        [SVProgressHUD dismiss];
                    }
                    
                    NSError *coreDataError;
                    [[Utility managedObjectContext] save:&coreDataError];
                    if(coreDataError){
                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                        [SVProgressHUD dismiss];
                        
                    }
                    else{
                        NSLog(@"DATA SAVED");
                        
                        NSMutableArray *checks = [customerDict objectForKey:@"checks"];
                        __block int checker = 0;
                        
                        
                        for (int j = 0 ; j < checks.count ; j++) {
                            Checks *check = [checks objectAtIndex:j];
                            ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];
                            
                            newCheck.bank = check.bank;
                            newCheck.checkAmount = check.checkAmount;
                            newCheck.checkNumber = check.checkNumber;
                            newCheck.checkType = check.checkType;
                            newCheck.company = check.company;
                            
                            newCheck.dueAmount = check.dueAmount;
                            newCheck.flatCharges = check.flatCharges;
                            newCheck.netCharges = check.netCharges;
                            newCheck.netDueAmount = check.netDueAmount;
                            newCheck.ratedChargesType = check.ratedChargesType;
                            newCheck.isSync = NO;
                            newCheck.lastUpdate = check.created;
                            newCheck.remarks = check.remarks;
                            newCheck.uploadStatus = @"new";
                            
                            checker = checker + 1;
                            
                            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:check.frontPhoto] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                
                                if (image && finished) {
                                    // Cache image to disk or memory
                                    
                                    NSLog(@"Size of Image Downloaded checPhoto(bytes):%lu",(unsigned long)[data length]);
                                    if (!error) {
                                        newCheck.checkPhoto = UIImageJPEGRepresentation(image, 0.8);
                                    }
                                    else{
                                        NSLog(@"ERROR = %@" , error.description);
                                        [SVProgressHUD dismiss];
                                    }
                                    [newCustomer addChecksObject:newCheck];

                                    NSError *coreDataErrorChecks;
                                    [[Utility managedObjectContext] save:&coreDataErrorChecks];
                                    if(coreDataErrorChecks){
                                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                                        [SVProgressHUD dismiss];
                                        checker = checker - 1;
                                        
                                        
                                    }
                                    else{
                                        [SVProgressHUD dismiss];
                                        NSLog(@"DATA CEHCKS SAVED");
                                        checker = checker - 1;
                                    }
                                    
                                    if (checker == 0) {
                                        

                                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30.0 * NSEC_PER_SEC));
                                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            [self saveInCoreDataWithCustomer:(index + 1)];
                                        });
                                    }
                                    
                                }
                            }];
                        }
                    }
                    
                }
            }];
        
        }
    }];
}




-(void)syncFromBackendless {
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    allCustomersInCoreData = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [SVProgressHUD showWithStatus:@"Syncing Data..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [self getAllCustomers];
}


-(void)getAllChecksWithAllCustomers:(NSArray *)allCustomers {
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    DataQueryBuilder *query = [DataQueryBuilder new];
    [query setPageSize:100];
    [dataStore find:query response:^(NSArray *checks) {
        [SVProgressHUD dismiss];
        NSLog(@"Checks Gotten = %lu" , (unsigned long)checks.count);
        NSLog(@"allCustomersInCoreData Gotten = %lu" , (unsigned long)allCustomersInCoreData.count);

        for (Customer *customer in allCustomers) {
            
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.customerId contains[cd] %@",customer.objectId];
            NSArray *filteredCkecks = [checks filteredArrayUsingPredicate:bPredicate];
            
            
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.firstName contains[cd] %@" , customer.firstName];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.lastName contains[cd] %@", customer.lastName];
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
 
            NSArray *filteredCustomers = [allCustomersInCoreData filteredArrayUsingPredicate:predicate];
            
            
            if (filteredCustomers.count) {
                NSLog(@"Customer Already Exists = %@" , customer.firstName);
            }
            else{
                [self saveCustomerInCoreDataWithCustomer:customer andChecks:filteredCkecks];
            }
        }
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)saveCustomerInCoreDataWithCustomer:(Customer *)customer andChecks :(NSArray *)checks{
    

    
    [SVProgressHUD showWithStatus:@"Syncing Data..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];
    newCustomer.identificationNumber =  customer.identificationNumber;
    newCustomer.socialSecurity =  customer.socialSecurity;
    newCustomer.firstName =  customer.firstName;
    newCustomer.lastName =  customer.lastName;
    newCustomer.address =  customer.address;
    newCustomer.city =  customer.city;
    newCustomer.state =  customer.state;
    newCustomer.zip =  customer.zip;
    newCustomer.homeContact =  customer.homeContact;
    newCustomer.officeContact =  customer.officeContact;
    newCustomer.cellContact =  customer.cellContact;
    newCustomer.remarks =  customer.remarks;
    newCustomer.isBlackListed = [customer.isBlackListed containsString:@"YES"];
    newCustomer.isSync = NO;
    newCustomer.lastUpdate = [NSDate date];
    newCustomer.uploadStatus = @"new";
    
    
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView sd_setImageWithURL:[NSURL URLWithString:customer.idPicture] placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (!error) {
            newCustomer.idPicture = UIImageJPEGRepresentation(image, 0.5);
        }
        else{
            
        }
        [imageView sd_setImageWithURL:[NSURL URLWithString:customer.profilePicture] placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (!error) {
                newCustomer.profilePicture = UIImageJPEGRepresentation(image, 0.5);
            }
            else{
                NSLog(@"ERROR = %@" , error.description);
                [SVProgressHUD dismiss];
            }
            NSError *coreDataError;
            [[Utility managedObjectContext] save:&coreDataError];
            if(coreDataError){
                NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                [SVProgressHUD dismiss];

            }
            else{
                NSLog(@"DATA SAVED");
                
                
                
                for (Checks *check in checks) {
                    ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];
                    
                    newCheck.bank = check.bank;
                    newCheck.checkAmount = check.checkAmount;
                    newCheck.checkNumber = check.checkNumber;
                    newCheck.checkType = check.checkType;
                    newCheck.company = check.company;
                    
                    newCheck.dueAmount = check.dueAmount;
                    newCheck.flatCharges = check.flatCharges;
                    newCheck.netCharges = check.netCharges;
                    newCheck.netDueAmount = check.netDueAmount;
                    newCheck.ratedChargesType = check.ratedChargesType;
                    newCheck.isSync = NO;
                    newCheck.lastUpdate = [NSDate date];
                    newCheck.uploadStatus = @"new";
                    
                    
                    [imageView sd_setImageWithURL:[NSURL URLWithString:check.frontPhoto] placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            newCheck.checkPhoto = UIImageJPEGRepresentation(image, 0.8);
                        }
                        else{
                            NSLog(@"ERROR = %@" , error.description);
                            [SVProgressHUD dismiss];
                        }
                        [newCustomer addChecksObject:newCheck];

                        NSError *coreDataErrorChecks;
                        [[Utility managedObjectContext] save:&coreDataErrorChecks];
                        if(coreDataErrorChecks){
                            NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                            [SVProgressHUD dismiss];

                        }
                        else{
                            [SVProgressHUD dismiss];
                            NSLog(@"DATA CEHCKS SAVED");
                        }
                            
                        }];
                }
                
            }
        }];
    }];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    NSError *error;
    [[Utility managedObjectContext] save:&error];
    
    if(error){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check with the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }
    else{
       
        

    }
}





- (IBAction)changePasswordAction:(UIButton *)sender {
    
    [self syncFromBackendless];
    
    return;
    
    
    
    
    
    
    
    
    
    
    
    
    
    if(self.passwordTxt.text.length == 0 || self.confirmPasswordtxt.text.length == 0 ){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Incomplete" andText:@"Please type Password and Confirm Password" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else{
        
        if(![self.passwordTxt.text isEqualToString:self.confirmPasswordtxt.text]){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"Password and Confirm Password are not Same" andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
        }
        else{
            [SVProgressHUD showWithStatus:@"Changing Password..."];
            [self.view endEditing:YES];
            
            BackendlessUser *user = backendless.userService.currentUser;
            
            
            user.password = self.passwordTxt.text;
            id<IDataStore> dataStore = [backendless.data of:[BackendlessUser class]];

            
            [dataStore save:user response:^(id newUser) {

                
                
                NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                [preferences setValue:self.confirmPasswordtxt.text forKey:@"RecordPassword"];
                [preferences synchronize];
                
                
                
                self.confirmPasswordtxt.text = @"";
                self.passwordTxt.text = @"";

                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Password has been changed" andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                //[backendless.userService logout];
                backendless.userService.currentUser = nil;
                [self performSegueWithIdentifier:@"ToLogout" sender:self];                

            } error:^(Fault *error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                [SVProgressHUD dismiss];
            }];
            
        }
    }
}
@end
