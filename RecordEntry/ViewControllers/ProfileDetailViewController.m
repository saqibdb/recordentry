//
//  ProfileDetailViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ProfileDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "AddNewCheckTableViewController.h"
#import "CustomerHistoryViewController.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "Checks.h"
#import "RecordEntry-Swift.h"
#import "ChecksCollectionViewCell.h"



@interface ProfileDetailViewController (){
    NSMutableArray *allChecks;
    Test *testGlobal;
}

@end

@implementation ProfileDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    testGlobal = [Test new];
    
    self.navigationItem.title = @"Detail";
    
    if (self.customer.isBlackListed == YES) {
        [self.blackListBtn setTitle:@"BlackListed"];
        [self.blackListBtn setTintColor:[UIColor redColor]];
    }
    else {
    }
    
    if (_isAddingCheck == NO) {
        [self.cancelBtn setTitle:@"Email Customer Information" forState:UIControlStateNormal];
        self.addNewCheckSpecial.hidden = NO;
    }
    else{
        [self.cancelBtn setTitle:@"Add New Check for Customer" forState:UIControlStateNormal];
        self.addNewCheckSpecial.hidden = YES;
    }
    /*
    [self.idImageView sd_setShowActivityIndicatorView:YES];
    [self.idImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.idImageView sd_setImageWithURL:[NSURL URLWithString:self.customer.idPicture]
                           placeholderImage:[UIImage imageNamed:@"placeHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   self.idImageView.image = image;
                               }
                               else{
                                   self.idImageView.image = [UIImage imageNamed:@"placeHolder"];
                               }
                               [self.view layoutIfNeeded];
                           }];
    
    
    [self.profileImageView sd_setShowActivityIndicatorView:YES];
    [self.profileImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.customer.profilePicture]
                           placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   self.profileImageView.image = image;
                               }
                               else{
                                   self.profileImageView.image = [UIImage imageNamed:@"avatarPH"];
                               }
                               [self.view layoutIfNeeded];
                           }];
    */
    
    
    self.idImageView.image = [UIImage imageWithData:self.customer.idPicture];
    self.profileImageView.image = [UIImage imageWithData:self.customer.profilePicture];

    
    self.nameTxt.text = self.customer.firstName;
    
    self.firstNameText.text = [NSString stringWithFormat:@"Customer First Name : %@",self.customer.firstName];
    self.lastNameText.text = [NSString stringWithFormat:@"Customer Last Name : %@",self.customer.lastName];
    self.addressText.text = [NSString stringWithFormat:@"Customer Address : %@",self.customer.address];

    
    
    
    
    self.addressTxt.text = self.customer.address;
    self.stateTxt.text = self.customer.state;
    self.cityTxt.text = self.customer.city;
    self.zipTxt.text = self.customer.zip;
    self.dlIdTxt.text = self.customer.identificationNumber;
    self.ssnTxt.text = self.customer.socialSecurity;
    
    self.homeCnTxt.text = self.customer.homeContact;
    self.officeCnTxt.text = self.customer.officeContact;
    self.cellCnTxt.text = self.customer.cellContact;
    self.remarksTxt.text = self.customer.remarks;
    
    self.homePhoneTxt.text = self.customer.homeContact;
    
    self.officePhoneTxt.text = self.customer.officeContact;
    self.cellPhoneTxt.text = self.customer.cellContact;

    
    [self.view layoutIfNeeded];

    //self.checksCarousel.delegate = self;
    //self.checksCarousel.dataSource = self;
    //self.checksCarousel.type = iCarouselTypeCoverFlow2;
    
    self.checksCollectionView.delegate = self;
    self.checksCollectionView.dataSource = self;
    
    //[self.checksCarousel reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated {
    [self getAllCustomerChecks];
}

-(void)viewDidAppear:(BOOL)animated {
    if (self.customer.isBlackListed == YES) {
        //[SANotificationView showSATinyBannerWithMessage:@"This Customer is BlackListed" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor]];
        //[SANotificationView showSATinyBannerWithMessage:@"This Customer is BlackListed" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor]];
        //[self.checksCarousel reloadData];

        [SANotificationView showSAStatusBarBannerWithMessage:@"This Customer is BlackListed" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor] showTime:100];
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    //[SANotificationView removeSATinyBanner];
    [SANotificationView removeSATinyBanner];
}

-(void)getAllCustomerChecks {
    allChecks = [self.customer.checks allObjects];
    self.totalTransactionsText.text = [NSString stringWithFormat:@"%lu" , (unsigned long)allChecks.count];
    
    
    if (allChecks.count) {
        ChecksCD *firstCheck = [allChecks lastObject];
        ChecksCD *lastCheck = [allChecks firstObject];
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //Optionally for time zone conversions
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];
        
        
        
        
        self.firstTransactionText.text = [formatter stringFromDate:firstCheck.lastUpdate];
        self.lastTransactionText.text = [formatter stringFromDate:lastCheck.lastUpdate];
        
    }
    
    
    //TODO place the last and first transaction here.
    
    //[self.checksCarousel reloadData];
    [self.checksCollectionView reloadData];
    
    
    
    
    /*
    if (!allChecks.count) {
        [SVProgressHUD showWithStatus:@"Getting Customer Checks..."];
    }
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    
    DataQueryBuilder *query = [DataQueryBuilder new];
    NSString *whereClause = [NSString stringWithFormat:@"customerId = '%@'",self.customer.objectId];
    
    [query setSortBy:@[@"created DESC"]];
    [query setWhereClause:whereClause];
    
    [dataStore find:query response:^(NSArray *checks) {
        [SVProgressHUD dismiss];
        allChecks = [[NSMutableArray alloc] initWithArray:checks];
        self.totalTransactionsText.text = [NSString stringWithFormat:@"%lu" , (unsigned long)checks.count];
        
        
        if (checks.count) {
            Checks *firstCheck = [checks lastObject];
            Checks *lastCheck = [checks firstObject];
            
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd-MMM-yyyy"];
            //Optionally for time zone conversions
            [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];

            
            
            
            self.firstTransactionText.text = [formatter stringFromDate:firstCheck.created];
            self.lastTransactionText.text = [formatter stringFromDate:lastCheck.created];

        }
        
        
        //TODO place the last and first transaction here.
        
        //[self.checksCarousel reloadData];
        [self.checksCollectionView reloadData];
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
    
*/
    
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return allChecks.count ;

}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ChecksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChecksCollectionViewCell" forIndexPath:indexPath];
    
    
    
    UIView *rootController = cell.contentBorderView;
    
    ChecksCD *check = allChecks[indexPath.row];
    
    
    
    UIImageView *checkImageView = [rootController viewWithTag:1] ;
    UILabel *dateText = [rootController viewWithTag:2] ;
    UILabel *companyText = [rootController viewWithTag:3] ;
    UILabel *bankText = [rootController viewWithTag:4] ;
    UILabel *checkTypeText = [rootController viewWithTag:5] ;
    UILabel *checkNumberText = [rootController viewWithTag:6] ;
    UILabel *checkAmountText = [rootController viewWithTag:7] ;
    UILabel *chargeTypeText = [rootController viewWithTag:8] ;
    UILabel *chargesText = [rootController viewWithTag:9] ;
    UILabel *remarksText = [rootController viewWithTag:10] ;
    
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"E, d MMM yyyy HH:mm:ss"];
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];
    NSString *stringFromDate = [formatter stringFromDate:check.lastUpdate];
    dateText.text = [NSString stringWithFormat:@"Dated : %@",stringFromDate];
    
    companyText.text = [NSString stringWithFormat:@"Company : %@",check.company];
    bankText.text = [NSString stringWithFormat:@"Bank : %@",check.bank];
    checkTypeText.text = [NSString stringWithFormat:@"Check Type : %@",check.checkType];
    checkNumberText.text = [NSString stringWithFormat:@"Check Number : %@",check.checkNumber];
    checkAmountText.text = [NSString stringWithFormat:@"Check Amount : %@",check.checkAmount];
    chargeTypeText.text = [NSString stringWithFormat:@"Charge Type : %@",check.ratedChargesType];
    chargesText.text = [NSString stringWithFormat:@"Charges : %@",check.netCharges];
    //remarksText.text = [NSString stringWithFormat:@"Remarks : %@",check.remarks];
    
    
    
    /*
    
    [checkImageView sd_setShowActivityIndicatorView:YES];
    [checkImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [checkImageView sd_setImageWithURL:[NSURL URLWithString:check.frontPhoto]
                      placeholderImage:[UIImage imageNamed:@"placeHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                          if (!error) {
                              checkImageView.image = image;
                          }
                          else{
                              checkImageView.image = [UIImage imageNamed:@"placeHolder"];
                          }
                          [rootController layoutIfNeeded];
                      }];
     */
    checkImageView.image = [UIImage imageWithData:check.checkPhoto];

    [rootController layoutIfNeeded];
    
    
    
    
    
    return cell;
    
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return allChecks.count ;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    UIView *rootController = [[[NSBundle mainBundle] loadNibNamed:@"CarouselPopUp" owner:self options:nil] objectAtIndex:0];
    rootController.frame = CGRectMake(0, 0, self.checksCarousel.frame.size.width, 366.0);
    
    Checks *check = allChecks[index];
    
    
    
    UIImageView *checkImageView = [rootController viewWithTag:1] ;
    UILabel *dateText = [rootController viewWithTag:2] ;
    UILabel *companyText = [rootController viewWithTag:3] ;
    UILabel *bankText = [rootController viewWithTag:4] ;
    UILabel *checkTypeText = [rootController viewWithTag:5] ;
    UILabel *checkNumberText = [rootController viewWithTag:6] ;
    UILabel *checkAmountText = [rootController viewWithTag:7] ;
    UILabel *chargeTypeText = [rootController viewWithTag:8] ;
    UILabel *chargesText = [rootController viewWithTag:9] ;
    UILabel *remarksText = [rootController viewWithTag:10] ;

    
   

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"E, d MMM yyyy HH:mm:ss"];
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];
    NSString *stringFromDate = [formatter stringFromDate:check.created];
    dateText.text = [NSString stringWithFormat:@"Dated : %@",stringFromDate];
    
    companyText.text = [NSString stringWithFormat:@"Company : %@",check.company];
    bankText.text = [NSString stringWithFormat:@"Bank : %@",check.bank];
    checkTypeText.text = [NSString stringWithFormat:@"Check Type : %@",check.checkType];
    checkNumberText.text = [NSString stringWithFormat:@"Check Number : %@",check.checkNumber];
    checkAmountText.text = [NSString stringWithFormat:@"Check Amount : %@",check.checkAmount];
    chargeTypeText.text = [NSString stringWithFormat:@"Charge Type : %@",check.ratedChargesType];
    chargesText.text = [NSString stringWithFormat:@"Charges : %@",check.netCharges];
    remarksText.text = [NSString stringWithFormat:@"Remarks : %@",check.remarks];



    
    
    [checkImageView sd_setShowActivityIndicatorView:YES];
    [checkImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [checkImageView sd_setImageWithURL:[NSURL URLWithString:check.frontPhoto]
                        placeholderImage:[UIImage imageNamed:@"placeHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if (!error) {
                                checkImageView.image = image;
                            }
                            else{
                                checkImageView.image = [UIImage imageNamed:@"placeHolder"];
                            }
                            [rootController layoutIfNeeded];
                        }];
    
    [rootController layoutIfNeeded];
    
    
    return rootController ;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    
    
    UIView *rootController = carousel.currentItemView;
    
    UIImageView *checkImageView = [rootController viewWithTag:1] ;
    [testGlobal showTheFullImageWithImageView:checkImageView viewController:self];

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChecksCollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UIView *rootController = cell.contentBorderView;
    UIImageView *checkImageView = [rootController viewWithTag:1] ;
    [testGlobal showTheFullImageWithImageView:checkImageView viewController:self];
}





- (IBAction)cancelAction:(id)sender {
    if (_isAddingCheck == NO) {
        [testGlobal showTheFullImage2WithCustomer:self.customer viewController:self];
    }
    else{
        [self performSegueWithIdentifier:@"ToAddCheck" sender:self];
    }
    //[self performSegueWithIdentifier:@"ToAddCheck" sender:self];
    
    

    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToAddCheck"]) {
        AddNewCheckTableViewController *vc = segue.destinationViewController;
        vc.customer = self.customer;
    }
    
    if ([segue.identifier isEqualToString:@"ToCustomerHistory"]) {
        CustomerHistoryViewController *vc = segue.destinationViewController;
        vc.customer = self.customer;
        vc.checks = allChecks;
    }
}


- (IBAction)customerHistoryAction:(id)sender {
    [self performSegueWithIdentifier:@"ToCustomerHistory" sender:self];
}

- (IBAction)registerFingerPrintAction:(id)sender {
}

- (IBAction)blackListAction:(id)sender {
    if (self.customer.isBlackListed == YES) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Confirm" message:@"Do You Want to Remove This Customer from BlackList?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            self.customer.isBlackListed = @"NO";
            if (![self.customer.uploadStatus isEqualToString:@"new"]) {
                self.customer.uploadStatus = @"update";
            }
            
            NSError *coreDataError;
            [[Utility managedObjectContext] save:&coreDataError];
            
            if(coreDataError){
                NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
            }
            else{
                NSLog(@"DATA SAVED");
            }
        }];
        
        UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"you pressed No, thanks button");
        }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Confirm" message:@"Do You Want to Add This Customer to BlackList?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            self.customer.isBlackListed = @"YES";
            if (![self.customer.uploadStatus isEqualToString:@"new"]) {
                self.customer.uploadStatus = @"update";
            }

            NSError *coreDataError;
            [[Utility managedObjectContext] save:&coreDataError];
            
            if(coreDataError){
                NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
            }
            else{
                NSLog(@"DATA SAVED");
            }
        }];
        
        UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"you pressed No, thanks button");
        }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

- (IBAction)profilePictureAction:(UIButton *)sender {
    
    
    
    [testGlobal showTheFullImageWithImageView:self.profileImageView viewController:self];
    
}

- (IBAction)idCardPictureAction:(UIButton *)sender {
    [testGlobal showTheFullImageWithImageView:self.idImageView viewController:self];
}

- (IBAction)addNewCheckSpecialAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToAddCheck" sender:self];
}

- (IBAction)deleteCustomerAction:(UIButton *)sender {
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Confirmation!" andText:@"Are you sure you want to delete the Customer and all information?" andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        if(btton == alertView.defaultButton) {
            NSLog(@"Default");
            
            
            [SVProgressHUD showWithStatus:@"Deleting..."];
            [self removeChecks];
            [backendless.fileService remove:self.customer.idPicture response:nil error:nil];
            [backendless.fileService remove:self.customer.profilePicture response:nil error:nil];
            id<IDataStore> dataStore = [backendless.data of:[Customer class]];
            [dataStore remove:self.customer response:^(NSNumber *number) {
                [SVProgressHUD dismiss];
                
                [self.navigationController popViewControllerAnimated:YES];
                
                
                
            } error:^(Fault *error) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }];
            
            
            
        }
        else {
            NSLog(@"Others");
            
        }
    }];
    [alert.defaultButton setTitle:@"YES" forState:UIControlStateNormal];
    [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 8]];
    
    [alert.cancelButton setTitle:@"NO" forState:UIControlStateNormal];
    [alert.cancelButton.titleLabel setFont: [alert.cancelButton.titleLabel.font fontWithSize: 8]];
    
    alert.cornerRadius = 3.0f;
    [alert show];
    
    
    
    
    
    
}
-(void)removeChecks {
    if(allChecks.count){
        for(Checks *check in allChecks){
            [backendless.fileService remove:check.frontPhoto response:nil error:nil];
            id<IDataStore> dataStoreChecks = [backendless.data of:[Checks class]];
            [dataStoreChecks remove:check response:nil error:nil];
        }
    }
}



@end
