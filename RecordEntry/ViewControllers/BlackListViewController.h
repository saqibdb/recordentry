//
//  BlackListViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 12/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlackListViewController : UIViewController<UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *customerTableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;


- (IBAction)searchButtonTextFieldClicked:(UITextField *)sender;



@end
