//
//  AddNewCheckTableViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "AddNewCheckTableViewController.h"
#import "Checks.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <RMStepsController/RMStepsController.h>
#import "EnrollNewTableViewController.h"
#import "IdentificationCaptureViewController.h"
#import "PhotocaptureViewController.h"
#import "CaptureChequePhotoView.h"
#import "ILSCheck.h"





@interface AddNewCheckTableViewController (){
    DVSwitch *ratedChargesSwitch;
    UIImage *frontCheckImage;
    UIImage *backCheckImage;
    
    
    
    UIImage *identificationImage;
    UIImage *customerPhoto;

    
}

@end

@implementation AddNewCheckTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
   
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, -20, 0);

    
    self.customerNameText.text = [NSString stringWithFormat:@"%@ %@",self.customer.firstName , self.customer.lastName];
    self.customerNameText.enabled = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(frontImageAction:)];
    [tapRecognizer setNumberOfTouchesRequired:1];

    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    self.frontPhotoImageView.userInteractionEnabled = YES;
    [self.frontPhotoImageView addGestureRecognizer:tapRecognizer];
    
    
    UITapGestureRecognizer *tapRecognizerBack = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backImageAction:)];
    [tapRecognizerBack setNumberOfTouchesRequired:1];
    self.backPhotoImageView.userInteractionEnabled = YES;
    [self.backPhotoImageView addGestureRecognizer:tapRecognizerBack];
    
}
-(void) frontImageAction:(id) sender{
    [self actionLaunchAppCameraWithTag:1];
}

-(void) backImageAction:(id) sender{
    [self actionLaunchAppCameraWithTag:2];
}


-(void)actionLaunchAppCameraWithTag :(int)tag{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        imagePicker.view.tag = tag;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                       message:@"Unable to find a camera on your device."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo {
    if (picker.view.tag == 1) {
        self.frontPhotoImageView.image = image;
        frontCheckImage = image;
    }
    if (picker.view.tag == 2) {
        self.backPhotoImageView.image = image;
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}



-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"Width is %f",self.switchView.frame.size.width);
    [self.view layoutIfNeeded];
    ratedChargesSwitch = [[DVSwitch alloc] initWithStringsArray:@[@"Percentage (%)", @"Fixed ($)"]];
    ratedChargesSwitch.frame = CGRectMake(0, 0, 250, self.switchView.frame.size.height);
    [self.switchView addSubview:ratedChargesSwitch];
    [self.view layoutIfNeeded];
    
    
    if(self.stepsController){
        NSArray *stepsControllers = self.stepsController.childViewControllers;
        for (UIViewController *controller in stepsControllers) {
            if ([controller isKindOfClass:[EnrollNewTableViewController class]]) {
                NSLog(@"Got the Controller");
                EnrollNewTableViewController *vc = (EnrollNewTableViewController *)controller;
                self.customer = vc.customer;
            }
            else if([controller isKindOfClass:[IdentificationCaptureViewController class]]){
                IdentificationCaptureViewController *vc = (IdentificationCaptureViewController *)controller;
                identificationImage = vc.identificationImage;
            }
            else if([controller isKindOfClass:[PhotocaptureViewController class]]){
                PhotocaptureViewController *vc = (PhotocaptureViewController *)controller;
                customerPhoto = vc.customerPhoto;
            }
            else if([controller isKindOfClass:[CaptureChequePhotoView class]]){
                CaptureChequePhotoView *vc = (CaptureChequePhotoView *)controller;
                frontCheckImage = vc.checkPhoto;
            }
            else{
                NSLog(@"Not");
            }
        }
    }
    
    
    
    self.customerNameText.text = [NSString stringWithFormat:@"%@ %@",self.customer.firstName , self.customer.lastName];
    self.customerNameText.enabled = NO;
    self.frontPhotoImageView.image = frontCheckImage;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 
*/

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.textColor = [UIColor colorWithRed:(124.0/255.0) green:(194.0/255.0) blue:(255.0/255.0) alpha:1.0];
}

- (IBAction)saveAction:(id)sender {
    
    if ((self.checkAmountText.text.length == 0) || (self.companyText.text.length == 0) || (self.bankText.text.length == 0) || (self.checkTypeText.text.length == 0) || (self.remarksText.text.length == 0) || (self.ratedChargeValueText.text.length == 0) || (self.flatChargesText.text.length == 0) || (self.netChargesText.text.length == 0) || (self.dueAmountText.text.length == 0) || (self.netDueAmount.text.length == 0)){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"In complete" andText:@"Please Fill all the details." andCancelButton:NO forAlertType:AlertInfo];
        //[alert show];
        //return;
    }
    
    if ((frontCheckImage == nil) || (backCheckImage == nil)) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"In complete" andText:@"Please Take Front and Back Photo Of check." andCancelButton:NO forAlertType:AlertInfo];
        //[alert show];
        //return;
    }
    
    
    if (self.stepsController){
        NSLog(@"Coming from the Steps");
        
        
        
        
        
        
        
        
        
        [self saveCustomerInCoreData];
        
        return;
        
        
        
        
        //TODO SAVE CUSTOMER HERE
        [self.saveBtn setEnabled:NO];
        
        [SVProgressHUD showWithStatus:@"Uploading Identification Image on Cloud..." ];
        
        NSData *imageData = UIImageJPEGRepresentation(identificationImage, 0.8);
        NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
        NSString *filePathToBeSaved = [NSString stringWithFormat:@"/IdentificationPictures/%fId.jpg",epoch];
        [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
            [self.saveBtn setEnabled:YES];
            [SVProgressHUD dismiss];
            self.customer.idPicture = newFile.fileURL;
            [self uploadProfilePicture];
            
        } error:^(Fault *fileError) {
            [self.saveBtn setEnabled:YES];
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
        
        
        
    }
    else{
        
        
        ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];
        
        newCheck.bank = self.bankText.text;
        newCheck.checkAmount = self.checkAmountText.text;
        newCheck.checkNumber = self.checkNumberText.text;
        newCheck.checkType = self.checkTypeText.text;
        newCheck.company = self.companyText.text;
        
        newCheck.dueAmount = self.dueAmountText.text;
        newCheck.flatCharges = self.flatChargesText.text;
        newCheck.netCharges = self.netChargesText.text;
        newCheck.netDueAmount = self.netDueAmount.text;
        newCheck.ratedChargesType = @[@"Percentage (%)", @"Fixed ($)"][ratedChargesSwitch.selectedIndex];;
        newCheck.checkPhoto = UIImageJPEGRepresentation([self imageWithImage:self.frontPhotoImageView.image scaledToWidth:512.0], 0.8);
        newCheck.backendlessId = [ILSCheck MD5HexDigest:newCheck.checkPhoto];

        newCheck.isSync = NO;
        newCheck.lastUpdate = [NSDate date];
        newCheck.uploadStatus = @"new";
        
        
        [self.customer addChecksObject:newCheck];
        
        NSError *error;
        [[Utility managedObjectContext] save:&error];
        
        if(error){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check with the Developers." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
        else{
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"A new Check Has been Created" andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
        
        
        
        
        
        
        
        
        
        /*
        Checks *newCheck = [Checks new];
        
        
        newCheck.bank = self.bankText.text;
        newCheck.checkAmount = self.checkAmountText.text;
        newCheck.checkNumber = self.checkNumberText.text;
        newCheck.checkType = self.checkTypeText.text;
        newCheck.company = self.companyText.text;
        newCheck.customerId = self.customer.objectId;
        newCheck.dueAmount = self.dueAmountText.text;
        newCheck.flatCharges = self.flatChargesText.text;
        newCheck.netCharges = self.netChargesText.text;
        newCheck.netDueAmount = self.netDueAmount.text;
        newCheck.ratedChargesType = @[@"Percentage (%)", @"Fixed ($)"][ratedChargesSwitch.selectedIndex];;
        
        [self uploadFrontCheckPictureWithCheck:newCheck];*/
    }
}


-(void)saveCustomerInCoreData{
    
    
    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];

    
    newCustomer.identificationNumber =  self.customer.identificationNumber;
    newCustomer.socialSecurity =  self.customer.socialSecurity;
    newCustomer.firstName =  self.customer.firstName;
    newCustomer.lastName =  self.customer.lastName;
    
    newCustomer.address =  self.customer.address;
    newCustomer.city =  self.customer.city;
    newCustomer.state =  self.customer.state;
    newCustomer.zip =  self.customer.zip;
    newCustomer.homeContact =  self.customer.homeContact;
    newCustomer.officeContact =  self.customer.officeContact;
    newCustomer.cellContact =  self.customer.cellContact;
    newCustomer.remarks =  self.customer.remarks;
    newCustomer.isBlackListed = NO;
    newCustomer.isSync = NO;
    newCustomer.lastUpdate = [NSDate date];
    newCustomer.uploadStatus = @"new";
    
    newCustomer.idPicture = UIImageJPEGRepresentation(identificationImage, 0.5);
    newCustomer.profilePicture = UIImageJPEGRepresentation(customerPhoto, 0.5);

    newCustomer.backendlessId = [ILSCheck MD5HexDigest:newCustomer.profilePicture];

    
    ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];

    newCheck.bank = self.bankText.text;
    newCheck.checkAmount = self.checkAmountText.text;
    newCheck.checkNumber = self.checkNumberText.text;
    newCheck.checkType = self.checkTypeText.text;
    newCheck.company = self.companyText.text;

    newCheck.dueAmount = self.dueAmountText.text;
    newCheck.flatCharges = self.flatChargesText.text;
    newCheck.netCharges = self.netChargesText.text;
    newCheck.netDueAmount = self.netDueAmount.text;
    newCheck.ratedChargesType = @[@"Percentage (%)", @"Fixed ($)"][ratedChargesSwitch.selectedIndex];;
    
    newCheck.checkPhoto = UIImageJPEGRepresentation([self imageWithImage:self.frontPhotoImageView.image scaledToWidth:512.0], 0.8);
    newCheck.backendlessId = [ILSCheck MD5HexDigest:newCheck.checkPhoto];

    newCheck.isSync = NO;
    newCheck.lastUpdate = [NSDate date];
    newCheck.uploadStatus = @"new";

    
    [newCustomer addChecksObject:newCheck];
    
    NSError *error;
    [[Utility managedObjectContext] save:&error];
    
    if(error){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check with the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"A new Check Has been Created" andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
    
    
    
}



-(void)createCustomer {
    [SVProgressHUD showWithStatus:@"Creating New Customer on Cloud..."];
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore save:self.customer response:^(id newCreatedRecord) {

        
        self.customer = (Customer *)newCreatedRecord;
        
        Checks *newCheck = [Checks new];
        
        
        newCheck.bank = self.bankText.text;
        newCheck.checkAmount = self.checkAmountText.text;
        newCheck.checkNumber = self.checkNumberText.text;
        newCheck.checkType = self.checkTypeText.text;
        newCheck.company = self.companyText.text;

        newCheck.dueAmount = self.dueAmountText.text;
        newCheck.flatCharges = self.flatChargesText.text;
        newCheck.netCharges = self.netChargesText.text;
        newCheck.netDueAmount = self.netDueAmount.text;
        newCheck.ratedChargesType = @[@"Percentage (%)", @"Fixed ($)"][ratedChargesSwitch.selectedIndex];;
        
        [self uploadFrontCheckPictureWithCheck:newCheck];
        

    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}



-(void)uploadProfilePicture {
    [SVProgressHUD showWithStatus:@"Uploading Profile Image on Cloud..."];
    
    NSData *imageData = UIImageJPEGRepresentation(customerPhoto, 0.8);
    NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
    NSString *filePathToBeSaved = [NSString stringWithFormat:@"/ProfilePictures/%fProfile.jpg",epoch];
    [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
        [SVProgressHUD dismiss];
        self.customer.profilePicture = newFile.fileURL;
        [self createCustomer];
        
    } error:^(Fault *fileError) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)uploadFrontCheckPictureWithCheck :(Checks *)check  {
    [SVProgressHUD showWithStatus:@"Uploading Checks Images on Cloud..."];
    NSData *imageData = UIImageJPEGRepresentation([self imageWithImage:self.frontPhotoImageView.image scaledToWidth:512.0], 0.8);
    NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
    NSString *filePathToBeSaved = [NSString stringWithFormat:@"/ChecksPictures/%fFront.jpg",epoch];
    [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
        [SVProgressHUD dismiss];
        check.frontPhoto = newFile.fileURL;
        [self createCheckWithCheck:check];

    } error:^(Fault *fileError) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)uploadBackCheckPictureWithCheck :(Checks *)check {
    [SVProgressHUD showWithStatus:@"Uploading Checks Images on Cloud..."];
    NSData *imageData = UIImageJPEGRepresentation([self imageWithImage:self.backPhotoImageView.image scaledToWidth:512.0], 0.8);
    NSTimeInterval epoch = [[NSDate date] timeIntervalSince1970];
    NSString *filePathToBeSaved = [NSString stringWithFormat:@"/ChecksPictures/%fBack.jpg",epoch];
    [backendless.file saveFile:filePathToBeSaved content:imageData overwriteIfExist:YES response:^(BackendlessFile *newFile) {
        [SVProgressHUD dismiss];
        check.backPhoto = newFile.fileURL;
        [self createCheckWithCheck:check];

    } error:^(Fault *fileError) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:fileError.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)createCheckWithCheck :(Checks *)check {
    [SVProgressHUD showWithStatus:@"Creating New Check on Cloud..."];
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    [dataStore save:check response:^(id newCreatedCheck) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"A new Check Has been Created in the Cloud" andCancelButton:NO forAlertType:AlertSuccess];
        [self.navigationController popViewControllerAnimated:YES];
        [alert show];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

/*
 @property (strong, nonatomic) NSString *backPhoto;
 @property (strong, nonatomic) NSString *bank;
 @property (strong, nonatomic) NSString *checkAmount;
 @property (strong, nonatomic) NSString *checkNumber;
 @property (strong, nonatomic) NSString *checkType;
 @property (strong, nonatomic) NSString *company;
 @property (strong, nonatomic) NSString *customerId;
 
 @property (strong, nonatomic) NSString *dueAmount;
 @property (strong, nonatomic) NSString *flatCharges;
 @property (strong, nonatomic) NSString *frontPhoto;
 @property (strong, nonatomic) NSString *netCharges;
 @property (strong, nonatomic) NSString *netDueAmount;
 @property (strong, nonatomic) NSString *ratedChargesType;
 
 
 
 
 
 
 @property (strong, nonatomic) NSString *ratedChargesValue;
 @property (strong, nonatomic) NSString *remarks;
 
 
 @property (strong, nonatomic) NSString *objectId;
 
 */

- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkAmountChanged:(UITextField *)sender {
    NSLog(@"Amount is %@",sender.text);
}

- (IBAction)addFrontPhotoAction:(UIButton *)sender {
    
    [self frontImageAction:sender];
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
