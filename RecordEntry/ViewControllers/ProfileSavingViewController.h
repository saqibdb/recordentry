//
//  ProfileSavingViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKSHorizontalLineProgressView/MKSHorizontalLineProgressView.h>
#import "Customer.h"



@interface ProfileSavingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *idImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameTxt;
@property (weak, nonatomic) IBOutlet UILabel *addressTxt;
@property (weak, nonatomic) IBOutlet UILabel *stateTxt;
@property (weak, nonatomic) IBOutlet UILabel *cityTxt;
@property (weak, nonatomic) IBOutlet UILabel *zipTxt;
@property (weak, nonatomic) IBOutlet UILabel *dlIdTxt;
@property (weak, nonatomic) IBOutlet UILabel *ssnTxt;
@property (weak, nonatomic) IBOutlet UILabel *homeCnTxt;
@property (weak, nonatomic) IBOutlet UILabel *officeCnTxt;
@property (weak, nonatomic) IBOutlet UILabel *cellCnTxt;
@property (weak, nonatomic) IBOutlet UITextView *remarksTxt;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet MKSHorizontalLineProgressView *progressView;



- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


@property (strong, nonatomic) Customer *customer;

@property (strong, nonatomic) UIImage *identificationImage;
@property (strong, nonatomic) UIImage *profileImage;

@end
