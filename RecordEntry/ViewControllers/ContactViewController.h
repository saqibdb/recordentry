//
//  ContactViewController.h
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LUNSegmentedControl/LUNSegmentedControl.h>



@interface ContactViewController : UIViewController<UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate, LUNSegmentedControlDataSource, LUNSegmentedControlDelegate>

@property (weak, nonatomic) IBOutlet UITextField *idText;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *addressText;
@property (weak, nonatomic) IBOutlet UITextField *fullNameText;
@property (weak, nonatomic) IBOutlet UITextField *checqueText;
@property (weak, nonatomic) IBOutlet UISearchBar *txtSearchBar;

@property (weak, nonatomic) IBOutlet UITableView *customerTableView;


@property (weak, nonatomic) IBOutlet UITextField *searchTextField;


@property (weak, nonatomic) IBOutlet LUNSegmentedControl *segmentedControl;
@property BOOL isAddingCheck;

- (IBAction)saveAction:(UIButton *)sender;

- (IBAction)searchEnded:(UITextField *)sender;

- (IBAction)searchButtonTextFieldClicked:(UITextField *)sender;


@end
