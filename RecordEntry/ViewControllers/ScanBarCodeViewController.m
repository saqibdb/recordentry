//
//  ScanBarCodeViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 14/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ScanBarCodeViewController.h"
#import <MTBBarcodeScanner/MTBBarcodeScanner.h>

#import <RMStepsController/RMStepsController.h>


@interface ScanBarCodeViewController (){
    MTBBarcodeScanner *scanner;
}

@end

@implementation ScanBarCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];

}

    
-(void)viewDidAppear:(BOOL)animated{
    
    
    
    scanner.didStartScanningBlock = ^{
        NSLog(@"The scanner started scanning! We can now hide any activity spinners.");
    };
    
    scanner.resultBlock = ^(NSArray *codes){
        NSLog(@"Found these codes: %@", codes);
    };
    
    scanner.didTapToFocusBlock = ^(CGPoint point){
        NSLog(@"The user tapped the screen to focus. \
              Here we could present a view at %@", NSStringFromCGPoint(point));
    };
    
    
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            
            NSError *error = nil;
            [scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                
            } error:&error];
            
        } else {
            NSLog(@"Permission Denied");
            // The user denied access to the camera
        }
    }];
}
    
    
-(void)viewWillDisappear:(BOOL)animated{
    [scanner stopScanning];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextAction:(id)sender {
    [self.stepsController showNextStep];
}
    @end
