//
//  IdentificationCaptureViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 16/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDPhotoCameraView.h"
#import "Customer.h"



@interface IdentificationCaptureViewController : UIViewController

@property (weak, nonatomic) IBOutlet BDPhotoCameraView *cameraView;

@property (weak, nonatomic) IBOutlet UIButton *captureBtn;



@property (strong, nonatomic) UIImage *identificationImage;



- (IBAction)captureAction:(id)sender;

- (IBAction)nextaction:(id)sender;



@end
