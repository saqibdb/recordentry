//
//  ViewController.m
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ViewController.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <AKVideoImageView/AKVideoImageView.h>




#import <CloudKit/CloudKit.h>
#import "ILSCloudKitManager.h"
#import "Student.h"

static NSString * const kCKRecordName = @"STUDENT";





@interface ViewController (){
    APLoadingButton *loginBtn;
    AKVideoImageView *videoBG;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSURL *videoURL = [[NSBundle mainBundle] URLForResource:@"698849988" withExtension:@"mp4"];
    videoBG = [[AKVideoImageView alloc] initWithFrame:self.view.bounds
                                                               videoURL:videoURL];
    [self.view addSubview:videoBG];
    [self.view sendSubviewToBack:videoBG];
    
    
    [self.view layoutIfNeeded];
    
    loginBtn = [[APLoadingButton alloc] initWithFrame:CGRectMake(self.passwordText.frame.origin.x, self.passwordText.frame.origin.y + 70, self.passwordText.frame.size.width, self.passwordText.frame.size.height)];
    [loginBtn setBackgroundColor:[UIColor colorWithRed:(100.0 / 255.0) green:(180.0 / 255.0) blue:(255.0 / 255.0) alpha:1.0]];
    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
    
    
    
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentLevelKeyPassword = @"RecordPassword";
    NSString *currentLevelKeyEmail = @"RecordEmail";

    if ([preferences objectForKey:currentLevelKeyPassword] != nil)    {
        self.passwordText.text = [preferences valueForKey:currentLevelKeyPassword];
    }
    if ([preferences objectForKey:currentLevelKeyEmail] != nil)    {
        self.emailText.text = [preferences valueForKey:currentLevelKeyEmail];
    }
    
    
    [self fetchStudents];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [videoBG removeFromSuperview];
    videoBG = nil;
}

-(void)viewDidAppear:(BOOL)animated{
    if (backendless.userService.currentUser) {
        [loginBtn succeedAnimationWithCompletion:^{
            [self performSegueWithIdentifier:@"ToDashboard" sender:self];
        }];
        return;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (IBAction)loginAction:(APLoadingButton *)sender {
    
    

    //[SVProgressHUD showWithStatus:@"Signing In..."];
    BackendlessUser *user = [BackendlessUser new];
    user.email = self.emailText.text;
    user.password = self.passwordText.text;
    [backendless.userService login:user.email password:user.password response:^(BackendlessUser *newUser) {
        //[SVProgressHUD dismiss];
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        [preferences setValue:self.passwordText.text forKey:@"RecordPassword"];
        [preferences setValue:self.emailText.text forKey:@"RecordEmail"];
        [preferences synchronize];
        
        
        [loginBtn succeedAnimationWithCompletion:^{
            [self performSegueWithIdentifier:@"ToDashboard" sender:self];
        }];
    } error:^(Fault *error) {
        //[SVProgressHUD dismiss];
        NSLog(@"Error = %@" , error.description);
        [loginBtn failedAnimationWithCompletion:^{
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
        
    }];
    
}


#pragma mark - Fetch students list from iCloud

- (void)fetchStudents {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@", kCKRecordName];
    [ILSCloudKitManager fetchAllRecordsWithType:kCKRecordName withPredicate:predicate CompletionHandler:^(NSArray *results, NSError *error) {
        
        if (error) {
            // Error handling for failed fetch from public database
        }
        else {
            // Display the fetched records
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSArray *students = [self mapStudents:results];
                NSLog(@"All Students = %lu" , (unsigned long)students.count);
                
            });
        }
    }];
}

#pragma mark - Map fetch result to student model object

- (NSArray *)mapStudents:(NSArray *)students {
    
    if (students.count == 0) return nil;
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    [students enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Student *student = [[Student alloc] initWithInputData:obj];
        [temp addObject:student];
    }];
    
    return [NSArray arrayWithArray:temp];
}



@end
