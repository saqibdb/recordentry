//
//  ScanBarCodeViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 14/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanBarCodeViewController : UIViewController

    
    @property (weak, nonatomic) IBOutlet UIView *scannerView;
    
    @property (weak, nonatomic) IBOutlet UIButton *nextBtn;
    
- (IBAction)nextAction:(id)sender;
    
    
    
@end
