//
//  ContactViewController.m
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ContactViewController.h"
#import "Record.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "CustomerTableViewCell.h"
#import "Customer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

#import "ProfileDetailViewController.h"
#import "SQBDisclosureIndicator.h"
#import "Checks.h"


#import "Utility.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>
#import <IQKeyboardManager/IQUIView+IQKeyboardToolbar.h>

@interface ContactViewController (){
    NSMutableArray *allCustomers;
    CustomerCD *selectedCustomer;
}

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.customerTableView.delegate = self;
    self.customerTableView.dataSource = self;
    self.segmentedControl.transitionStyle = LUNSegmentedControlTransitionStyleFade;
    self.segmentedControl.delegate = self;
    self.segmentedControl.dataSource = self;

    self.txtSearchBar.delegate = self;
    self.searchTextField.keyboardToolbar.doneBarButton.title = @"Search";
    [self.searchTextField.keyboardToolbar.doneBarButton setTarget:self action:@selector(searchButtonTextFieldClicked:)];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aNewUserAdded:) name:@"coreDataUpdatedRecordEntry" object:nil];
}
- (void) aNewUserAdded:(NSNotification *) notification {
    [self loadAllDataWithReload:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coreDataUpdatedRecordEntry" object:nil];
}


-(void)doneAction:(UITextField *)sender {
    NSLog(@"DONE CLICKED");
}


-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [self loadAllDataWithReload:YES];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self segmentedControl:self.segmentedControl didScrollWithXOffset:0];
}

-(void)loadAllDataWithReload :(BOOL)toReload
{
    
    
    
    
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    
    allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"firstName"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    allCustomers = [[allCustomers sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    
    

    
    
    
    
    if (toReload) {
        [self.customerTableView reloadData];
    }
    return;
    
    
    if (!allCustomers.count) {
        [SVProgressHUD showWithStatus:@"Getting Customers..."];
    }
    
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    
    [dataStore find:^(NSArray *customers) {
        [SVProgressHUD dismiss];
        allCustomers = [[NSMutableArray alloc] initWithArray:customers];
        
        [self.customerTableView reloadData];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)filterChecksWithSearchString:(NSString *)strSearch{
    NSString *whereClause = [NSString stringWithFormat:@"company LIKE '%%%@%%'", strSearch];
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setWhereClause:whereClause];
    
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    [dataStore find:queryBuilder
           response:^(NSArray *checks) {
               NSLog(@"Checks Result: %@", checks);
               NSMutableArray *allCustomerIds = [[NSMutableArray alloc] init];
               for (Checks *check in checks) {
                   NSString *customerId = check.customerId;
                   if (![allCustomerIds containsObject:customerId]) {
                       [allCustomerIds addObject:customerId];
                   }
               }
               if (allCustomerIds.count) {
                   NSString *allCustomerIdsString = [NSString stringWithFormat:@"objectId IN ('%@')",[allCustomerIds componentsJoinedByString:@"','"]];
                   NSLog(@"Where clause is %@" , allCustomerIdsString);
                   
                   DataQueryBuilder *queryBuilder2 = [[DataQueryBuilder alloc] init];
                   [queryBuilder2 setWhereClause:allCustomerIdsString];
                   
                   id<IDataStore> dataStore2 = [backendless.data of:[Customer class]];
                   [dataStore2 find:queryBuilder2
                          response:^(NSArray *customers) {
                              NSLog(@"Result: %@", customers);
                              
                              [SVProgressHUD dismiss];
                              allCustomers = [[NSMutableArray alloc] initWithArray:customers];
                              
                              [self.customerTableView reloadData];
                          }
                             error:^(Fault *error) {
                                 NSLog(@"Server reported an error: %@", error);
                                 [SVProgressHUD dismiss];
                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                                 [alert show];
                             }
                    ];
               }
               else{
                   [SVProgressHUD dismiss];
                   allCustomers = [[NSMutableArray alloc] init];
                   [self.customerTableView reloadData];

               }
               
               
               
               
               
               
               
               
           }
              error:^(Fault *error) {
                  NSLog(@"Server reported an error: %@", error);
                  [SVProgressHUD dismiss];
                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                  [alert show];
              }
     ];
}


-(void)filterDataWithSearchString:(NSString *)strSearch andColumn :(NSString *)column
{
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];
    NSString *whereClause = [NSString stringWithFormat:@"%@ LIKE '%%%@%%'",column, strSearch];
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setWhereClause:whereClause];
    
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore find:queryBuilder
           response:^(NSArray *customers) {
               NSLog(@"Result: %@", customers);
               
               [SVProgressHUD dismiss];
               allCustomers = [[NSMutableArray alloc] initWithArray:customers];
               
               [self.customerTableView reloadData];
           }
              error:^(Fault *error) {
                  NSLog(@"Server reported an error: %@", error);
                  [SVProgressHUD dismiss];
                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                  [alert show];
              }
     ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}



- (IBAction)saveAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"ToSteps" sender:self];
    return;
    
    if (self.idText.text.length == 0 || self.phoneText.text.length == 0 || self.addressText.text.length == 0 || self.fullNameText.text.length == 0 || self.checqueText.text.length == 0 ) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"All Fields are not filled. Please fill before continuing." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    [[self view] endEditing:YES];

    Record *newRecord = [Record new];
    
    newRecord.address = self.addressText.text ;
    newRecord.checqueNumber = self.checqueText.text ;
    newRecord.fullName = self.fullNameText.text ;
    newRecord.identification = self.idText.text ;
    newRecord.photo = @"NOT PRESENT" ;
    newRecord.photoChecque = @"NOT PRESENT" ;
    newRecord.phoneNumber = self.phoneText.text ;

    [SVProgressHUD showWithStatus:@"Creating New Record on Cloud..."];
    id<IDataStore> dataStore = [backendless.data of:[Record class]];

    [dataStore save:newRecord response:^(id newCreatedRecord) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"A new Record Has been Created in the Cloud. Please check the Search tab to search for it" andCancelButton:NO forAlertType:AlertSuccess];
        
        self.addressText.text = @"";
        self.checqueText.text  = @"";
        self.fullNameText.text  = @"";
        self.idText.text  = @"";
        self.phoneText.text  = @"";
        
        [alert show];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Check your internet connectivity or contact the Developers." andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
    
}

- (IBAction)searchEnded:(UITextField *)sender {
    
    return;
    if (sender.text.length > 0){
        if (self.segmentedControl.currentState == 0) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([customObject.firstName containsString:sender.text]);
            }]];
        }
        else if (self.segmentedControl.currentState == 1) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([customObject.identificationNumber containsString:sender.text]);
            }]];
        }
        else{
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                NSArray *checksArray = [customObject.checks allObjects];
                for (ChecksCD *checkObj in checksArray) {
                    if ([checkObj.company containsString:sender.text]) {
                        return YES;
                    }
                }
                return NO;
            }]];
        }
        
        
    }
    else{
        [self loadAllDataWithReload:YES];
    }
    
    /*
    [SVProgressHUD showWithStatus:@"Getting Customers..."];
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];
    
    if (sender.text.length > 0){
        if (self.segmentedControl.currentState == 0) {
            NSArray *names = [sender.text componentsSeparatedByString:@" "];
            if(names.count){
                [self filterDataWithSearchString:names[0] andColumn:@"firstName"];
            }
            else{
                [self filterDataWithSearchString:sender.text andColumn:@"firstName"];
            }
        }
        else if (self.segmentedControl.currentState == 0) {
            [self filterDataWithSearchString:sender.text andColumn:@"identificationNumber"];
        }
        else {
            
            
            [self filterChecksWithSearchString:sender.text];
            //[self filterDataWithSearchString:sender.text andColumn:@"address"];
        }
    }
    else{
        [self loadAllData];
    }
    */
    [sender resignFirstResponder];
}

- (IBAction)searchButtonTextFieldClicked:(UITextField *)sender {
    
    if (sender.text.length > 0){
        [self loadAllDataWithReload:NO];

        
        sender.text = [sender.text lowercaseString];
        if (self.segmentedControl.currentState == 0) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([[customObject.firstName lowercaseString] containsString:sender.text]);
            }]];
        }
        else if (self.segmentedControl.currentState == 1) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([[customObject.identificationNumber lowercaseString] containsString:sender.text]);
            }]];
        }
        else{
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                NSArray *checksArray = [customObject.checks allObjects];
                for (ChecksCD *checkObj in checksArray) {
                    if ([[checkObj.company lowercaseString] containsString:sender.text]) {
                        return YES;
                    }
                }
                return NO;
            }]];
        }
        
        
    }
    else{
        [self loadAllDataWithReload:YES];
    }
    [self.customerTableView reloadData];
    [sender resignFirstResponder];
    return;
    
    
    
    
    
    
    
    [SVProgressHUD showWithStatus:@"Getting Customers..."];
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];
    
    if (sender.text.length > 0){
        if (self.segmentedControl.currentState == 0) {
            NSArray *names = [sender.text componentsSeparatedByString:@" "];
            if(names.count){
                [self filterDataWithSearchString:names[0] andColumn:@"firstName"];
            }
            else{
                [self filterDataWithSearchString:sender.text andColumn:@"firstName"];
            }
            
        }
        else if (self.segmentedControl.currentState == 0) {
            [self filterDataWithSearchString:sender.text andColumn:@"identificationNumber"];
        }
        else {
            [self filterChecksWithSearchString:sender.text];

            //[self filterDataWithSearchString:sender.text andColumn:@"address"];
        }
    }
    else{
        [self loadAllDataWithReload:YES];
    }
    
    [sender resignFirstResponder];
}


#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allCustomers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CustomerTableViewCell";
    CustomerTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
    }
    
    
    CustomerCD *customer = allCustomers[indexPath.row];
    cell.customerName.text = [NSString stringWithFormat:@"%@ %@",customer.firstName , customer.lastName];
    
    /*
    [cell.customerAvatar sd_setShowActivityIndicatorView:YES];
    [cell.customerAvatar sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.customerAvatar sd_setImageWithURL:[NSURL URLWithString:customer.profilePicture]
                    placeholderImage:[UIImage imageNamed:@"avatarPH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            cell.customerAvatar.image = image;
                        }
                        else{
                            cell.customerAvatar.image = [UIImage imageNamed:@"avatarPH"];
                        }
                        [cell layoutIfNeeded];
                    }];
     */
    
    cell.customerAvatar.image = [UIImage imageWithData:customer.profilePicture];
    
    cell.accessoryView = [[SQBDisclosureIndicator alloc] initWithColor:[UIColor whiteColor]];

    [cell layoutIfNeeded];
    return cell ;
}//ToProfileView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedCustomer = allCustomers[indexPath.row];
    [self performSegueWithIdentifier:@"ToProfileView" sender:self];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length > 0){
        if (self.segmentedControl.currentState == 0) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([customObject.firstName containsString:searchBar.text]);
            }]];
        }
        else if (self.segmentedControl.currentState == 1) {
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                return ([customObject.identificationNumber containsString:searchBar.text]);
            }]];
        }
        else{
            [allCustomers filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                CustomerCD *customObject=(CustomerCD *) evaluatedObject;
                NSArray *checksArray = [customObject.checks allObjects];
                for (ChecksCD *checkObj in checksArray) {
                    if ([checkObj.company containsString:searchBar.text]) {
                        return YES;
                    }
                }
                return NO;
            }]];
        }


    }
    else{
        [self loadAllDataWithReload:YES];
    }
    /*
    [SVProgressHUD showWithStatus:@"Getting Customers..."];
    //NSString *whereClause = [NSString stringWithFormat:@"Name LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%' OR address LIKE '%%%@%%' OR socialSecurity LIKE '%%%@%%'",strSearch, strSearch, strSearch, strSearch];

    if (searchBar.text.length > 0){
        if (self.segmentedControl.currentState == 0) {
            NSArray *names = [searchBar.text componentsSeparatedByString:@" "];
            if(names.count){
                [self filterDataWithSearchString:names[0] andColumn:@"firstName"];
            }
            else{
                [self filterDataWithSearchString:searchBar.text andColumn:@"firstName"];
            }
            
        }
        else if (self.segmentedControl.currentState == 0) {
            [self filterDataWithSearchString:searchBar.text andColumn:@"identificationNumber"];
        }
        else {
            [self filterChecksWithSearchString:searchBar.text];
            //[self filterDataWithSearchString:searchBar.text andColumn:@"address"];
        }
    }
    else{
        [self loadAllData];
    }
    */
    
    
    
    [_txtSearchBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_txtSearchBar resignFirstResponder];
}




- (NSArray<UIColor *> *)segmentedControl:(LUNSegmentedControl *)segmentedControl gradientColorsForStateAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return @[[UIColor colorWithRed:160 / 255.0 green:223 / 255.0 blue:56 / 255.0 alpha:1.0], [UIColor colorWithRed:177 / 255.0 green:255 / 255.0 blue:0 / 255.0 alpha:1.0]];
            
            break;
            
        case 1:
            return @[[UIColor colorWithRed:78 / 255.0 green:252 / 255.0 blue:208 / 255.0 alpha:1.0], [UIColor colorWithRed:51 / 255.0 green:199 / 255.0 blue:244 / 255.0 alpha:1.0]];
            break;
            
        case 2:
            return @[[UIColor colorWithRed:178 / 255.0 green:0 / 255.0 blue:235 / 255.0 alpha:1.0], [UIColor colorWithRed:233 / 255.0 green:0 / 255.0 blue:147 / 255.0 alpha:1.0]];
            break;
            
        default:
            break;
    }
    return nil;
}

- (NSInteger)numberOfStatesInSegmentedControl:(LUNSegmentedControl *)segmentedControl {
    return 3;
}

- (NSAttributedString *)segmentedControl:(LUNSegmentedControl *)segmentedControl attributedTitleForStateAtIndex:(NSInteger)index {
    if (index == 0) {
        return [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];
    }
    else if(index == 1) {
        return [[NSAttributedString alloc] initWithString:@"ID" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];
    }
    else{
        return [[NSAttributedString alloc] initWithString:@"Company" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];

    }
}

- (NSAttributedString *)segmentedControl:(LUNSegmentedControl *)segmentedControl attributedTitleForSelectedStateAtIndex:(NSInteger)index {

    if (index == 0) {
        return [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:16]}];
    }
    else if(index == 1) {
        return [[NSAttributedString alloc] initWithString:@"ID" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:16]}];
    }
    else{
        return [[NSAttributedString alloc] initWithString:@"Company" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:16]}];
        
    }
}


- (void)segmentedControl:(LUNSegmentedControl *)segmentedControl didScrollWithXOffset:(CGFloat)offset {
    
    
}
- (void)segmentedControl:(LUNSegmentedControl *)segmentedControl didChangeStateFromStateAtIndex:(NSInteger)fromIndex toStateAtIndex:(NSInteger)toIndex{
    if (toIndex == 0) {
        self.txtSearchBar.placeholder = @"Find in Name";
        self.searchTextField.placeholder = @"Find in Name";
    }
    else if(toIndex == 1) {
        self.txtSearchBar.placeholder = @"Find in ID Number";
        self.searchTextField.placeholder = @"Find in ID Number";
    }
    else{
        self.txtSearchBar.placeholder = @"Find in Company";
        self.searchTextField.placeholder = @"Find in Company";
    }
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"ToProfileView"]) {
         ProfileDetailViewController *vc = segue.destinationViewController;
         vc.customer = selectedCustomer;
         vc.isAddingCheck = self.isAddingCheck;
     }
 }

@end
