//
//  CustomerHistoryViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 20/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACFloatingTextfield_Objc/ACFloatingTextField.h>
#import "Customer.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>


@interface CustomerHistoryViewController : UIViewController<UITableViewDelegate , UITableViewDataSource>



@property (weak, nonatomic) IBOutlet ACFloatingTextField *customerNameText;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;


@property (strong, nonatomic) CustomerCD *customer;
@property (strong, nonatomic) NSMutableArray *checks;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateText;
@property (weak, nonatomic) IBOutlet UILabel *companyText;
@property (weak, nonatomic) IBOutlet UILabel *bankText;
@property (weak, nonatomic) IBOutlet UILabel *checkTypeText;
@property (weak, nonatomic) IBOutlet UILabel *checkNumberText;
@property (weak, nonatomic) IBOutlet UILabel *checkAmountText;
@property (weak, nonatomic) IBOutlet UILabel *chargeTypeText;
@property (weak, nonatomic) IBOutlet UILabel *chargesTExt;
@property (weak, nonatomic) IBOutlet UILabel *remarksText;




@end
