//
//  EnrollNewTableViewController.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 14/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACFloatingTextfield_Objc/ACFloatingTextField.h>
#import "Customer.h"



@interface EnrollNewTableViewController : UITableViewController<UITableViewDelegate>
    @property (weak, nonatomic) IBOutlet UIButton *previousBtn;
    @property (weak, nonatomic) IBOutlet UIButton *nextBtn;
    


@property (weak, nonatomic) IBOutlet ACFloatingTextField *idText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *socialSecurityText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *nameText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *lastNameText;



@property (weak, nonatomic) IBOutlet ACFloatingTextField *addressText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *cityText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *stateText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *zipText;

@property (weak, nonatomic) IBOutlet ACFloatingTextField *homeContacttext;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *officeContactText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *cellContactText;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *remarksText;

@property (strong, nonatomic) Customer *customer;

- (IBAction)previousAction:(id)sender;
- (IBAction)nextAction:(id)sender;
    
    
    
@end
