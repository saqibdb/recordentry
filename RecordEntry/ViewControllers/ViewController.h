//
//  ViewController.h
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APLoadingButton.h"


@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

- (IBAction)loginAction:(APLoadingButton *)sender;



@end

