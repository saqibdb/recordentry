//
//  CustomerHistoryViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 20/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "CustomerHistoryViewController.h"
#import "CustomerHistoryTableViewCell.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "Checks.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>


@interface CustomerHistoryViewController ()

@end

@implementation CustomerHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.historyTableView.delegate = self;
    self.historyTableView.dataSource = self;
    
    
    self.customerNameText.text = [NSString stringWithFormat:@"%@ %@",self.customer.firstName , self.customer.lastName];
    self.customerNameText.enabled = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegates


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.checks.count;
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CustomerHistoryTableViewCell";
    CustomerHistoryTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CustomerHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
    }
    
    ChecksCD *check = self.checks[indexPath.row];
    cell.checkNumberText.text = check.checkNumber;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMM-yyyy"];
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];
    NSString *stringFromDate = [formatter stringFromDate:check.lastUpdate];
    
    
    
    cell.checkDateText.text = stringFromDate;
    
    
    
    return cell ;
}//ToProfileView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ChecksCD *check = self.checks[indexPath.row];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"E, d MMM yyyy HH:mm:ss"];
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"est"]];
    NSString *stringFromDate = [formatter stringFromDate:check.lastUpdate];
    self.dateText.text = [NSString stringWithFormat:@"Dated : %@",stringFromDate];
    
    self.companyText.text = [NSString stringWithFormat:@"Company : %@",check.company];
    self.bankText.text = [NSString stringWithFormat:@"Bank : %@",check.bank];
    self.checkTypeText.text = [NSString stringWithFormat:@"Check Type : %@",check.checkType];
    self.checkNumberText.text = [NSString stringWithFormat:@"Check Number : %@",check.checkNumber];
    self.checkAmountText.text = [NSString stringWithFormat:@"Check Amount : %@",check.checkAmount];
    self.chargeTypeText.text = [NSString stringWithFormat:@"Charge Type : %@",check.ratedChargesType];
    self.chargesTExt.text = [NSString stringWithFormat:@"Charges : %@",check.netCharges];
    self.remarksText.text = [NSString stringWithFormat:@"Remarks : %@",check.remarks];
    self.checkImageView.image = [UIImage imageWithData:check.checkPhoto];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
