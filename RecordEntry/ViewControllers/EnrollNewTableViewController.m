//
//  EnrollNewTableViewController.m
//  RecordEntry
//
//  Created by iBuildx-Macbook on 14/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "EnrollNewTableViewController.h"
#import <RMStepsController/RMStepsController.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "RecordEntry-Swift.h"
@interface EnrollNewTableViewController ()

@end

@implementation EnrollNewTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view layoutIfNeeded];

    //self..edgesForExtendedLayout = UIRectEdgeNone;
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, -20, 0);
    //self.tableView.delegate = self;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *idText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *socialSecurityText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *nameText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *addressText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *cityText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *stateText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *zipText;
 
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *homeContacttext;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *officeContactText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *cellContactText;
 @property (weak, nonatomic) IBOutlet ACFloatingTextField *remarksText;
*/



- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.textColor = [UIColor colorWithRed:(124.0/255.0) green:(194.0/255.0) blue:(255.0/255.0) alpha:1.0];
}

- (IBAction)previousAction:(id)sender {
    [self.stepsController showPreviousStep];
}
    
- (IBAction)nextAction:(id)sender {
    if ((self.idText.text.length == 0) || (self.socialSecurityText.text.length == 0) || (self.nameText.text.length == 0) || (self.addressText.text.length == 0) || (self.cityText.text.length == 0) || (self.stateText.text.length == 0) || (self.zipText.text.length == 0) || (self.homeContacttext.text.length == 0) || (self.officeContactText.text.length == 0) || (self.cellContactText.text.length == 0) || (self.remarksText.text.length == 0)) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"In complete" andText:@"Please Fill all the details." andCancelButton:NO forAlertType:AlertInfo];
        //[alert show];
        //return;
    }
    
    
    self.customer = [Customer new];
    
    self.customer.identificationNumber = self.idText.text;
    self.customer.socialSecurity = self.socialSecurityText.text;
    self.customer.firstName = self.nameText.text;
    self.customer.lastName = self.lastNameText.text;

    self.customer.address = self.addressText.text;
    self.customer.city = self.cityText.text;
    self.customer.state = self.stateText.text;
    self.customer.zip = self.zipText.text;
    self.customer.homeContact = self.homeContacttext.text;
    self.customer.officeContact = self.officeContactText.text;
    self.customer.cellContact = self.cellContactText.text;
    self.customer.remarks = self.remarksText.text;
    [self.stepsController showNextStep];
}
    @end
