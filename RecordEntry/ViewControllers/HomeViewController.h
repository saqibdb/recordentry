//
//  HomeViewController.h
//  RecordEntry
//
//  Created by Maulik Vekariya on 12/26/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>



@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBtn;

@property (weak, nonatomic) IBOutlet CCMBorderView *menuView1;

@property (weak, nonatomic) IBOutlet UIButton *syncbtn;


@property (weak, nonatomic) IBOutlet CCMBorderView *syncView;




@property BOOL isAddingCheck;




- (IBAction)menuAction:(id)sender;




- (IBAction)menu1Action:(UIButton *)sender;
- (IBAction)menu2Action:(UIButton *)sender;
- (IBAction)menu3Action:(UIButton *)sender;
- (IBAction)menu4Action:(UIButton *)sender;
- (IBAction)menu5Action:(UIButton *)sender;
- (IBAction)menu6Action:(UIButton *)sender;
- (IBAction)menu7Action:(UIButton *)sender;

- (IBAction)syncIcloudAction:(UIButton *)sender;






@end
