//
//  CaptureChequePhotoView.m
//  RecordEntry
//
//  Created by Maulik Vekariya on 12/28/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "CaptureChequePhotoView.h"
#import <RMStepsController/RMStepsController.h>
#import "EnrollNewTableViewController.h"
#import <Backendless.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "IdentificationCaptureViewController.h"
#import "ProfileSavingViewController.h"


@interface CaptureChequePhotoView (){
    UIImage *gottenImage;
}
@end

@implementation CaptureChequePhotoView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.cameraView initAgain];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.cameraView stopCamera];
}

- (IBAction)captureAction:(id)sender {
    
    [self.cameraView takePhotoWithCompletion:^(UIImage *image) {
        
        if (image) {
            NSLog(@"Got the Image");
            
            UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.cameraView.frame.size.width, self.cameraView.frame.size.height)];
            CGSize sz ;
            if (image.size.width < image.size.height) {
                sz = CGSizeMake(image.size.width, image.size.width);
            }
            else{
                sz = CGSizeMake(image.size.height, image.size.height);
            }
            sz = CGSizeMake(512.0, 512.0);
            
            iv.image = [self squareImageWithImage:image scaledToSize:sz];
            gottenImage = iv.image;
            
            self.checkPhoto = gottenImage;

            
            [self.cameraView stopCamera];
            [self.cameraView addSubview:iv];
        }
    }];
    
}

- (IBAction)previousAction:(id)sender {
    if (gottenImage) {
        gottenImage = nil;
        [self.cameraView initAgain];
    }
    [self.stepsController showPreviousStep];
}

- (IBAction)nextAction:(id)sender
{
    
    if (!gottenImage) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Picture" andText:@"Please Take a Photo of Customer to proceed." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else
    {
        [self.stepsController showNextStep];
//        [self performSegueWithIdentifier:@"ToSaveProfile" sender:self];
    }
    
    
    return;
    UIImage *identificationImage;
    
    //IdentificationCaptureViewController
    NSArray *stepsControllers = self.stepsController.childViewControllers;
    for (UIViewController *controller in stepsControllers) {
        if ([controller isKindOfClass:[EnrollNewTableViewController class]]) {
            NSLog(@"Got the Controller");
            EnrollNewTableViewController *vc = (EnrollNewTableViewController *)controller;
            self.customer = vc.customer;
        }
        else if([controller isKindOfClass:[IdentificationCaptureViewController class]]){
            IdentificationCaptureViewController *vc = (IdentificationCaptureViewController *)controller;
            identificationImage = vc.identificationImage;
        }
        else{
            NSLog(@"Not");
        }
    }
    
}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
