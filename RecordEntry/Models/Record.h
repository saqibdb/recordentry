//
//  Record.h
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Record : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *checqueNumber;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *identification;
@property (strong, nonatomic) NSString *photo;
@property (strong, nonatomic) NSString *photoChecque;
@property (strong, nonatomic) NSString *phoneNumber;

//phoneNumber
@end
