//
//  Checks.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 19/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Checks : NSObject

@property (strong, nonatomic) NSString *backPhoto;
@property (strong, nonatomic) NSString *bank;
@property (strong, nonatomic) NSString *checkAmount;
@property (strong, nonatomic) NSString *checkNumber;
@property (strong, nonatomic) NSString *checkType;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *customerId;

@property (strong, nonatomic) NSString *dueAmount;
@property (strong, nonatomic) NSString *flatCharges;
@property (strong, nonatomic) NSString *frontPhoto;
@property (strong, nonatomic) NSString *netCharges;
@property (strong, nonatomic) NSString *netDueAmount;
@property (strong, nonatomic) NSString *ratedChargesType;






@property (strong, nonatomic) NSString *ratedChargesValue;
@property (strong, nonatomic) NSString *remarks;


@property (strong, nonatomic) NSString *objectId;

@property (strong, nonatomic) NSDate *created;


@end
