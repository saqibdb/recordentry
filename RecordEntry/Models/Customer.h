//
//  Customer.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 15/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject


@property (strong, nonatomic) NSString *identificationNumber;
@property (strong, nonatomic) NSString *socialSecurity;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *zip;

@property (strong, nonatomic) NSString *homeContact;
@property (strong, nonatomic) NSString *officeContact;
@property (strong, nonatomic) NSString *cellContact;
@property (strong, nonatomic) NSString *remarks;
@property (strong, nonatomic) NSString *profilePicture;
@property (strong, nonatomic) NSString *idPicture;

@property (strong, nonatomic) NSString *isBlackListed;


@property (strong, nonatomic) NSString *objectId;

@end
