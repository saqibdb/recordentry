//
//  Family.h
//  FnF
//
//  Created by iBuildx-Macbook on 20/01/2018.
//  Copyright © 2018 saqibdb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Utility : NSObject


+ (NSManagedObjectContext *)managedObjectContext ;

@end
