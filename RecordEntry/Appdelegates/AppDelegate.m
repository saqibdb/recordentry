//
//  AppDelegate.m
//  RecordEntry
//
//  Created by ibuildx on 9/27/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "AppDelegate.h"
#import <Backendless.h>
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>



#import <Backendless.h>
#import "Customer.h"

#import "Checks.h"

#import "Utility.h"


#import "CustomerCD+CoreDataClass.h"
#import "CustomerCD+CoreDataProperties.h"

#import "ChecksCD+CoreDataClass.h"
#import "ChecksCD+CoreDataProperties.h"
#import <CoreData/CoreData.h>

#import "ILSCheck.h"
#import "ILSCustomer.h"
#import "ILSCloudKitManager.h"
#import <CloudKit/CloudKit.h>


@interface AppDelegate (){
    NSMutableArray *allCustomersInCoreData;
    
    NSMutableArray *allBackendlessCustomers;
    NSMutableArray *allBackendlessChecks;
    
    NSMutableArray *allCustomersChecksMapped;
    
    
    
    NSMutableArray *newICLOUDCustomers;
    NSMutableArray *newICLOUDChecks;
    
    NSMutableArray *newICLOUDCustomersChecksMapped;
    NSMutableArray *allCoreDataRecordsNew;

}

@end

@implementation AppDelegate


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [backendless initApp:@"FB5AE073-8A07-7897-FF49-1C3AEFDCC000" APIKey:@"CD8D289C-5EC8-154B-FF92-FDC41337EA00"];

    [[IQKeyboardManager sharedManager] setEnable:YES];
    [Fabric with:@[[Crashlytics class]]];

    [self checkCustomerForsync];  //Used for sync NEW Core data to cloud
    
    [self checkCheckForsync];  //Used for sync NEW Core data to cloud
    
    
    [self checkCustomerUpdateForsync];  //Used for sync UPDATED Core data to cloud
    
    [self checkCheckUpdateForsync];  //Used for sync UPDATED Core data to cloud
    
    [self getICloudsCustomersDataForLocal];

    return YES;
}

-(void)getICloudsCustomersDataForLocal {
    [ILSCloudKitManager fetchRecordsWithType:@"Customer" completionHandler:^(NSArray *records, NSError *error) {
        if (error) {
            NSLog(@"ERROR AT ICLOUD - %@" , error.description);
        }
        else{
            newICLOUDCustomers =  [[self mapCustomers:records] mutableCopy];
            [self getICloudsChecksDataForLocal];
        }
    }];
}


-(void)getICloudsChecksDataForLocal {
    
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    __block NSMutableArray *allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"Total Customers Gotten = %lu" , (unsigned long)allCustomers.count);
    
    [ILSCloudKitManager fetchRecordsWithType:@"Check" completionHandler:^(NSArray *records, NSError *error) {
        if (error) {
            NSLog(@"ERROR AT ICLOUD - %@" , error.description);
        }
        else{
            newICLOUDChecks =  [[self mapChecks:records] mutableCopy];
        
            for (ILSCustomer *customerILS in newICLOUDCustomers) {
                NSString *cloudSpecialId = [ILSCheck MD5HexDigest:customerILS.idPicture];
                BOOL locallyFound = NO;
                for (CustomerCD *customerCD in allCustomers) {
                    NSString *coreSpecialId = [ILSCheck MD5HexDigest:customerCD.idPicture];
                    if ([cloudSpecialId isEqualToString:coreSpecialId]) {
                        NSLog(@"ALready Present");
                        locallyFound = YES;
                        break;
                    }
                }
                if (locallyFound == NO) {
                    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];

                    [ILSCustomer createCustomerCDFromILS:customerILS withNewCreatedCustomerCD:newCustomer];

                    for (ILSCheck *checkILS in newICLOUDChecks) {
                        if ([checkILS.customerId isEqualToString:[ILSCheck MD5HexDigest:customerILS.idPicture]]) {
                            [newCustomer addChecksObject:[ILSCheck createCheckCDFromILS:checkILS]];
                        }
                    }
                    
                    NSLog(@"NEW CUSTOMER IN LOCAL ADDED = %@" , newCustomer.firstName);
                    NSError *coreDataError;
                    [[Utility managedObjectContext] save:&coreDataError];
                    if(coreDataError){
                        NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE CHECK= %@" , coreDataError.description);
                    }
                    else{
                        NSLog(@"DATA SAVED AT AFTER CLOUD SAVE CHECK");
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"coreDataUpdatedRecordEntry" object:nil];

                    }
                    
                }
            }
            
        }
    }];
}








-(void)checkCustomerForsync {
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadStatus == 'new'"];
    [fetchRequest setPredicate:predicate];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    NSMutableArray *allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"Total Customers Gotten = %lu" , (unsigned long)allCustomers.count);
    if (allCustomers.count) {
        [self saveCustomersInCloudWithCustomers:allCustomers withUpdate:NO];
    }
    else{
        NSLog(@"The Data is Upto Date with your iCloud.");
    }
}

-(void)saveCustomersInCloudWithCustomers :(NSArray *)customers withUpdate:(BOOL)isUpdate{
    NSMutableArray *customerRecords = [[NSMutableArray alloc] init];
    for (CustomerCD *customer in customers) {
        NSLog(@"Customer Name = %@" , customer.firstName);
        NSDictionary *object = [ILSCustomer createDictionaryFromCoreData:customer];
        NSString *fullName = [NSString stringWithFormat:@"%@ %@" , customer.firstName , customer.lastName];
        
        if (customer.backendlessId) {
            fullName = customer.backendlessId;
        }
        else{
            fullName = [fullName stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        }
        CKRecord *record1 = [[CKRecord alloc] initWithRecordType:@"Customer" recordID:[[CKRecordID alloc] initWithRecordName:fullName]];
        record1[@"identificationNumber"] = [object valueForKeyPath:ILSCustomerFields.identificationNumber];
        record1[@"socialSecurity"] = [object valueForKeyPath:ILSCustomerFields.socialSecurity];
        record1[@"firstName"] = [object valueForKeyPath:ILSCustomerFields.firstName];
        record1[@"lastName"] = [object valueForKeyPath:ILSCustomerFields.lastName];
        record1[@"address"] = [object valueForKeyPath:ILSCustomerFields.address];
        record1[@"city"] = [object valueForKeyPath:ILSCustomerFields.city];
        record1[@"state"] = [object valueForKeyPath:ILSCustomerFields.state];
        record1[@"zip"] = [object valueForKeyPath:ILSCustomerFields.zip];
        
        record1[@"homeContact"] = [object valueForKeyPath:ILSCustomerFields.homeContact];
        record1[@"officeContact"] = [object valueForKeyPath:ILSCustomerFields.officeContact];
        record1[@"cellContact"] = [object valueForKeyPath:ILSCustomerFields.cellContact];
        record1[@"remarks"]= [object valueForKeyPath:ILSCustomerFields.remarks];
        record1[@"profilePicture"] = [object valueForKeyPath:ILSCustomerFields.profilePicture];
        record1[@"idPicture"] = [object valueForKeyPath:ILSCustomerFields.idPicture];
        record1[@"isBlackListed"] = [object valueForKeyPath:ILSCustomerFields.isBlackListed];
        record1[@"objectId"] = [object valueForKeyPath:ILSCustomerFields.objectId];
        
        [customerRecords addObject:record1];
    }
    
    [ILSCloudKitManager saveRecords:customerRecords withUpdate:isUpdate withCompletionHandler:^(NSArray *savedRecords, NSError *error) {
        if(error) {
            NSLog(@"%@", error);
        } else {
            NSLog(@"Saved successfully");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                for (CustomerCD *customer in customers) {
                    customer.isSync = YES;
                    customer.uploadStatus = @"synced";
                }
                NSError *coreDataError;
                [[Utility managedObjectContext] save:&coreDataError];
                if(coreDataError){
                    NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE= %@" , coreDataError.description);
                }
                else{
                    NSLog(@"DATA SAVED AT AFTER CLOUD SAVE");
                }
                
                NSLog(@"Your Customers iCloud Successfully Saved..");

            });
        }
    } recordProgressHandler:^(double progress) {
        NSLog(@"Progress = %f" , progress);
    }];
}



-(void)checkCheckForsync {
    NSFetchRequest *fetchRequest = [ChecksCD fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadStatus == 'new'"];
    [fetchRequest setPredicate:predicate];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    NSMutableArray *allChecks = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"Total ChecksCD Gotten = %lu" , (unsigned long)allChecks.count);
    if (allChecks.count) {
        [self saveChecksInCloudWithCustomers:allChecks withUpdate:NO];
    }
    else{
        NSLog(@"The Data is Upto Date with your iCloud.");
    }
}


-(void)saveChecksInCloudWithCustomers :(NSArray *)checks withUpdate:(BOOL)isUpdate{
    NSMutableArray *checkRecords = [[NSMutableArray alloc] init];
    
    for (ChecksCD *check in checks) {
        NSDictionary *object = [ILSCheck createDictionaryFromCoreData:check andCustomerId:[ILSCheck MD5HexDigest:check.customer.idPicture]];
        
        
        CKRecord *record1 = [[CKRecord alloc] initWithRecordType:@"Check" recordID:[[CKRecordID alloc] initWithRecordName:[ILSCheck MD5HexDigest:check.customer.idPicture]]];
        
        
        record1[@"bank"] = [object valueForKeyPath:ILSCheckFields.bank];
        record1[@"checkAmount"] = [object valueForKeyPath:ILSCheckFields.checkAmount];
        record1[@"checkNumber"] = [object valueForKeyPath:ILSCheckFields.checkNumber];
        record1[@"checkPhoto"] = [object valueForKeyPath:ILSCheckFields.checkPhoto];
        record1[@"checkType"] = [object valueForKeyPath:ILSCheckFields.checkType];
        record1[@"company"] = [object valueForKeyPath:ILSCheckFields.company];
        record1[@"dueAmount"] = [object valueForKeyPath:ILSCheckFields.dueAmount];
        record1[@"flatCharges"] = [object valueForKeyPath:ILSCheckFields.flatCharges];
        record1[@"isSync"] = [object valueForKeyPath:ILSCheckFields.isSync];
        record1[@"lastUpdate"] = [object valueForKeyPath:ILSCheckFields.lastUpdate];
        record1[@"netCharges"] = [object valueForKeyPath:ILSCheckFields.netCharges];
        record1[@"netDueAmount"] = [object valueForKeyPath:ILSCheckFields.netDueAmount];
        record1[@"ratedChargesType"] = [object valueForKeyPath:ILSCheckFields.ratedChargesType];
        record1[@"remarks"] = [object valueForKeyPath:ILSCheckFields.remarks];
        
        record1[@"customerId"] = [ILSCheck MD5HexDigest:check.customer.idPicture];
        
        [checkRecords addObject:record1];
    }
    
    [ILSCloudKitManager saveRecords:checkRecords withUpdate:isUpdate withCompletionHandler:^(NSArray *savedRecords, NSError *error) {
        if(error) {
            NSLog(@"%@", error);
        } else {
            NSLog(@"Saved successfully");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                for (ChecksCD *check in checks) {
                    check.isSync = YES;
                    check.uploadStatus = @"synced";
                }
                NSError *coreDataError;
                [[Utility managedObjectContext] save:&coreDataError];
                if(coreDataError){
                    NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE= %@" , coreDataError.description);
                }
                else{
                    NSLog(@"DATA SAVED AT AFTER CLOUD SAVE");
                }
                
                NSLog(@"Your Customers iCloud Successfully Saved..");
                
            });
        }
    } recordProgressHandler:^(double progress) {
        NSLog(@"Progress = %f" , progress);
    }];
}





-(void)checkCustomerUpdateForsync {
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadStatus == 'update'"];
    [fetchRequest setPredicate:predicate];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    NSMutableArray *allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"Update Customers Gotten = %lu" , (unsigned long)allCustomers.count);
    if (allCustomers.count) {
        [self saveCustomersInCloudWithCustomers:allCustomers withUpdate:YES];
    }
    else{
        NSLog(@"The Data is Upto Date with your iCloud.");
    }
}

-(void)checkCheckUpdateForsync {
    NSFetchRequest *fetchRequest = [ChecksCD fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadStatus == 'update'"];
    [fetchRequest setPredicate:predicate];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    NSMutableArray *allCustomers = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"Update Checks Gotten = %lu" , (unsigned long)allCustomers.count);
    if (allCustomers.count) {
        [self saveChecksInCloudWithCustomers:allCustomers withUpdate:YES];
    }
    else{
        NSLog(@"The Data is Upto Date with your iCloud.");
    }
}





-(void)triggerSync {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self syncFromBackendless];
}

-(void)getAllCustomers {
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setPageSize:100];
    id<IDataStore> dataStore = [backendless.data of:[Customer class]];
    [dataStore find:queryBuilder response:^(NSArray *customers) {
        NSLog(@"Customers Gotten = %lu" , (unsigned long)customers.count);
        //[self getAllChecksWithAllCustomers:customers];
        
        allBackendlessCustomers = [[NSMutableArray alloc] initWithArray:customers];
        [self getAllChecks];
        
    } error:^(Fault *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

-(void)getAllChecks {
    id<IDataStore> dataStore = [backendless.data of:[Checks class]];
    DataQueryBuilder *query = [DataQueryBuilder new];
    [query setPageSize:100];
    [dataStore find:query response:^(NSArray *checks) {
        
        NSLog(@"Checks Gotten = %lu" , (unsigned long)checks.count);
        NSLog(@"allCustomersInCoreData Gotten = %lu" , (unsigned long)allCustomersInCoreData.count);
        
        allBackendlessChecks = [[NSMutableArray alloc] initWithArray:checks];
        
        [self mapCustomerWithCheck];
        
        
    } error:^(Fault *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}


-(void)mapCustomerWithCheck {
    
    allCustomersChecksMapped = [[NSMutableArray alloc] init];
    
    for (int i = 0 ; i < allBackendlessCustomers.count ; i++) {
        
        
        Customer *customer = allBackendlessCustomers[i];
        
        
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.customerId contains[cd] %@",customer.objectId];
        NSArray *filteredCkecks = [allBackendlessChecks filteredArrayUsingPredicate:bPredicate];
        
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.firstName contains[cd] %@" , customer.firstName];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.lastName contains[cd] %@", customer.lastName];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        
        NSArray *filteredCustomers = [allCustomersInCoreData filteredArrayUsingPredicate:predicate];
        
        
        if (filteredCustomers.count) {
            NSLog(@"Customer Already Exists = %@" , customer.firstName);
        }
        else{
            
            NSDictionary *mappedDict = @{ @"customer" : customer, @"checks" : filteredCkecks};
            
            [allCustomersChecksMapped addObject:mappedDict];
            
            //[self saveCustomerInCoreDataWithCustomer:customer andChecks:filteredCkecks];
        }
        
        if ((i+1) == allBackendlessCustomers.count) {
            [self saveInCoreDataWithCustomer:0];
            break;
        }
        else{
            NSLog(@"Total Count = %lu and index = %i" ,(unsigned long)allBackendlessCustomers.count , i );
        }
    }
}


-(void)saveInCoreDataWithCustomer :(int)index {
    
    if (index >= allCustomersChecksMapped.count) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"All Previous Data has been Synced with your iCloud." andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
        
        
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *currentLevelKeyOldDataSyncCheck= @"RecordOldDataSync";
        
        
        [preferences setObject:@"YES" forKey:currentLevelKeyOldDataSyncCheck];
        
        [preferences synchronize];
        
        
        
        return;
    }
    
    NSDictionary *customerDict = [allCustomersChecksMapped objectAtIndex:index];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    
    
    Customer *customer = [customerDict objectForKey:@"customer"];
    
    CustomerCD *newCustomer = [[CustomerCD alloc] initWithContext:[Utility managedObjectContext]];
    newCustomer.identificationNumber =  customer.identificationNumber;
    newCustomer.socialSecurity =  customer.socialSecurity;
    newCustomer.firstName =  customer.firstName;
    newCustomer.lastName =  customer.lastName;
    newCustomer.address =  customer.address;
    newCustomer.city =  customer.city;
    newCustomer.state =  customer.state;
    newCustomer.zip =  customer.zip;
    newCustomer.homeContact =  customer.homeContact;
    newCustomer.officeContact =  customer.officeContact;
    newCustomer.cellContact =  customer.cellContact;
    newCustomer.remarks =  customer.remarks;
    newCustomer.isBlackListed = [customer.isBlackListed containsString:@"YES"];
    newCustomer.isSync = NO;
    newCustomer.lastUpdate = [NSDate date];
    newCustomer.backendlessId = customer.objectId;
    
    
    
    
    NSLog(@"Called Before ImageDownload");
    
    
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.idPicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image && finished) {
            // Cache image to disk or memory
            
            NSLog(@"Size of Image Downloaded (bytes):%lu",(unsigned long)[data length]);
            if (!error) {
                newCustomer.idPicture = UIImageJPEGRepresentation(image, 0.5);
            }
            else{
                
            }
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:customer.profilePicture] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if (image && finished) {
                    // Cache image to disk or memory
                    
                    NSLog(@"Size of Image Downloaded profilePicture(bytes):%lu",(unsigned long)[data length]);
                    if (!error) {
                        newCustomer.profilePicture = UIImageJPEGRepresentation(image, 0.5);
                    }
                    else{
                        NSLog(@"ERROR = %@" , error.description);
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    }
                    
                    NSError *coreDataError;
                    [[Utility managedObjectContext] save:&coreDataError]; //CUSTOMER SAVING
                    
                    
                    NSDictionary *customerObj = [ILSCustomer createDictionaryFromCoreData:newCustomer];
                    __block NSString *fullName = [NSString stringWithFormat:@"%@ %@" , newCustomer.firstName , newCustomer.lastName];
                    
                    if (newCustomer.backendlessId) {
                        fullName = newCustomer.backendlessId;
                    }
                    else{
                        fullName = [fullName stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
                    }
                    
                    
                    [ILSCloudKitManager createRecord:customerObj WithRecordType:@"Customer" WithRecordId:fullName completionHandler:^(NSArray *results, NSError *error) {
                        if(error) {
                            NSLog(@"+++++++++++%@ %li", error.localizedDescription , (long)error.code);
                            if (error.code == 14) {
                                newCustomer.isSync = YES;
                            }
                            else{
                                newCustomer.isSync = NO;
                            }
                        } else {
                            NSLog(@"Saved successfully");
                            newCustomer.isSync = YES;
                        }
                        NSError *coreDataError;
                        [[Utility managedObjectContext] save:&coreDataError];
                        if(coreDataError){
                            NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE= %@" , coreDataError.description);
                        }
                        else{
                            NSLog(@"DATA SAVED AT AFTER CLOUD SAVE");
                        }
                    }];
                    
                    if(coreDataError){
                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

                    }
                    else{
                        NSLog(@"DATA SAVED");
                        
                        NSMutableArray *checks = [customerDict objectForKey:@"checks"];
                        __block int checker = 0;
                        
                        
                        for (int j = 0 ; j < checks.count ; j++) {
                            Checks *check = [checks objectAtIndex:j];
                            ChecksCD *newCheck = [[ChecksCD alloc] initWithContext:[Utility managedObjectContext]];
                            
                            newCheck.bank = check.bank;
                            newCheck.checkAmount = check.checkAmount;
                            newCheck.checkNumber = check.checkNumber;
                            newCheck.checkType = check.checkType;
                            newCheck.company = check.company;
                            
                            newCheck.dueAmount = check.dueAmount;
                            newCheck.flatCharges = check.flatCharges;
                            newCheck.netCharges = check.netCharges;
                            newCheck.netDueAmount = check.netDueAmount;
                            newCheck.ratedChargesType = check.ratedChargesType;
                            newCheck.isSync = NO;
                            newCheck.lastUpdate = check.created;
                            newCheck.remarks = check.remarks;
                            newCheck.backendlessId = check.objectId;
                            
                            checker = checker + 1;
                            
                            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:check.frontPhoto] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                
                                if (image && finished) {
                                    // Cache image to disk or memory
                                    
                                    NSLog(@"Size of Image Downloaded checPhoto(bytes):%lu",(unsigned long)[data length]);
                                    if (!error) {
                                        newCheck.checkPhoto = UIImageJPEGRepresentation(image, 0.8);
                                    }
                                    else{
                                        NSLog(@"ERROR = %@" , error.description);
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                    }
                                    [newCustomer addChecksObject:newCheck];
                                    
                                    NSError *coreDataErrorChecks;
                                    [[Utility managedObjectContext] save:&coreDataErrorChecks];
                                    
                                    
                                    
                                    
                                    

                                
                                    
                                    
                                    NSDictionary *checkObject = [ILSCheck createDictionaryFromCoreData:newCheck andCustomerId:fullName];
                                    
                                    [ILSCloudKitManager createRecord:checkObject WithRecordType:@"Check" WithRecordId:[ILSCheck MD5HexDigest:newCheck.checkPhoto] completionHandler:^(NSArray *results, NSError *error) {
                                        if(error) {
                                            NSLog(@"%@", error);
                                        } else {
                                            NSLog(@"Saved successfully");
                                            newCheck.isSync = YES;
                                            NSError *coreDataError;
                                            [[Utility managedObjectContext] save:&coreDataError];
                                            if(coreDataError){
                                                NSLog(@"CORE DATA ERROR AT AFTER CLOUD SAVE CHECK= %@" , coreDataError.description);
                                            }
                                            else{
                                                NSLog(@"DATA SAVED AT AFTER CLOUD SAVE CHECK");
                                            }
                                        }
                                    }];
                                    
                                    
                                    
                                    
                                    
                                    if(coreDataErrorChecks){
                                        NSLog(@"CORE DATA ERROR = %@" , coreDataError.description);
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                        checker = checker - 1;
                                        
                                        
                                    }
                                    else{
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                        NSLog(@"DATA CEHCKS SAVED");
                                        checker = checker - 1;
                                    }
                                    
                                    if (checker == 0) {
                                        
                                        
                                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15.0 * NSEC_PER_SEC));
                                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            [self saveInCoreDataWithCustomer:(index + 1)];
                                        });
                                    }
                                    
                                }
                                else{
                                    NSLog(@"Error At Image Upload = %@",error.localizedDescription);
                                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

                                }
                            }];
                        }
                    }
                    
                }
                else{
                    NSLog(@"Error At Image Upload = %@",error.localizedDescription);
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

                }
            }];
            
        }
        else{
            NSLog(@"Error At Image Upload = %@",error.localizedDescription);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        }
    }];
}




-(void)syncFromBackendless {
    NSFetchRequest *fetchRequest = [CustomerCD fetchRequest];
    if([Utility managedObjectContext] == nil){
        NSLog(@"ISSUE HERE");
    }
    allCustomersInCoreData = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    [self getAllCustomers];
}







- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if (backendless.userService.currentUser) {
        [backendless.userService logout];
    }
    [self saveContext];

}



#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"RecordEntry"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support




- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"RecordEntry" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DatabaseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Map fetch result to student model object

- (NSArray *)mapCustomers:(NSArray *)customers {
    
    if (customers.count == 0) return nil;
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    [customers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        ILSCustomer *customer = [[ILSCustomer alloc] initWithInputData:obj];
        [temp addObject:customer];
    }];
    
    return [NSArray arrayWithArray:temp];
}


- (NSArray *)mapChecks:(NSArray *)customers {
    
    if (customers.count == 0) return nil;
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    [customers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        ILSCheck *check = [[ILSCheck alloc] initWithInputData:obj];
        [temp addObject:check];
    }];
    
    return [NSArray arrayWithArray:temp];
}


@end
