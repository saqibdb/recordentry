//
//  CustomerTableViewCell.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 15/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *customerAvatar;
@property (weak, nonatomic) IBOutlet UILabel *customerName;




@end
