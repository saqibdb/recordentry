//
//  CustomerHistoryTableViewCell.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 20/11/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *checkNumberText;

@property (weak, nonatomic) IBOutlet UILabel *checkDateText;




@end
