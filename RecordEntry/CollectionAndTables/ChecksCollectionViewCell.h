//
//  ChecksCollectionViewCell.h
//  RecordEntry
//
//  Created by iBuildx-Macbook on 12/01/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>



@interface ChecksCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet CCMBorderView *contentBorderView;


@end
